//
//  FirstLaunchViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/7/8.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "FirstLaunchViewController.h"
#import "ContentViewController.h"
@interface FirstLaunchViewController ()<UIScrollViewDelegate>
{
    BOOL isOut;
}
@end

@implementation FirstLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isOut = NO;
    [self makeLaunchView];
    // Do any additional setup after loading the view.
}
//假引导页面
-(void)makeLaunchView{
    NSArray *arr=[NSArray arrayWithObjects:@"launch1.jpeg",@"launch2.jpeg",@"launch3.jpeg",@"launch4.jpg",nil];//数组内存放的是我要显示的假引导页面图片
    //通过scrollView 将这些图片添加在上面，从而达到滚动这些图片
    UIScrollView *scr=[[UIScrollView alloc] initWithFrame:self.view.bounds];
    scr.contentSize=CGSizeMake(Shitu_SCREENWIDTH*arr.count, Shitu_SCREENHEIGHT);
    scr.pagingEnabled=YES;
    scr.tag=7000;
    scr.delegate=self;
    [self.view addSubview:scr];
    
    for (int i=0; i<arr.count; i++) {
        UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(i*Shitu_SCREENWIDTH, 0, Shitu_SCREENWIDTH, Shitu_SCREENHEIGHT)];
        img.image=[UIImage imageNamed:arr[i]];
        [scr addSubview:img];
        img = nil;
    }
}
#pragma mark scrollView的代理
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //这里是在滚动的时候判断 我滚动到哪张图片了，如果滚动到了最后一张图片，那么
    //如果在往下面滑动的话就该进入到主界面了，我这里利用的是偏移量来判断的，当
    //一共五张图片，所以当图片全部滑完后 又像后多滑了30 的时候就做下一个动作
    if (scrollView.contentOffset.x>3*Shitu_SCREENWIDTH+20) {
        
        isOut=YES;//这是我声明的一个全局变量Bool 类型的，初始值为no，当达到我需求的条件时将值改为yes
        
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //判断isout为真 就要进入主界面了
    if (isOut) {
        //这里添加了一个动画，
        [UIView animateWithDuration:1.5 animations:^{
            scrollView.alpha=0;//让scrollview 渐变消失
            
        }completion:^(BOOL finished) {
            [scrollView  removeFromSuperview];//将scrollView移除
            [self gotoMain];//进入主界面
            
        }];
    }
    
}
-(void)gotoMain{
    //如果第一次进入没有文件，我们就创建这个文件
    //判断 我是否创建了文件，如果没创建 就创建这个文件（这种情况就运行一次，也就是第一次启动程序的时候）
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"isEverLaunched"]) {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isEverLaunched"];
    }
    ContentViewController *contentVc = [[ContentViewController alloc]init];
    [self.navigationController pushViewController:contentVc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
