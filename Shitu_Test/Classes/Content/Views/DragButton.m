//
//  UIButton+NMCategory.m
//  DragButtonDemo
//
//  Created by Aster0id on 14-5-16.
//
//

#import "DragButton.h"
#import <objc/runtime.h>
#define PADDING     5
static void *DragEnableKey = &DragEnableKey;
static void *AdsorbEnableKey = &AdsorbEnableKey;

@implementation DragButton


-(void)setDragEnable:(BOOL)dragEnable
{
    objc_setAssociatedObject(self, DragEnableKey,@(dragEnable), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isDragEnable
{
    return [objc_getAssociatedObject(self, DragEnableKey) boolValue];
}

-(void)setAdsorbEnable:(BOOL)adsorbEnable
{
    objc_setAssociatedObject(self, AdsorbEnableKey,@(adsorbEnable), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isAdsorbEnable
{
    return [objc_getAssociatedObject(self, AdsorbEnableKey) boolValue];
}

CGPoint beginPoint;
CGPoint originPoint;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.highlighted = YES;
    if (![objc_getAssociatedObject(self, DragEnableKey) boolValue]) {
        return;
    }

    UITouch *touch = [touches anyObject];
    
    beginPoint = [touch locationInView:self];
    originPoint = CGPointMake(self.frame.origin.x, self.frame.origin.y);
    [self.dragDelegate beginDragButtonWithTouchPoint:beginPoint];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.highlighted = NO;
    if (![objc_getAssociatedObject(self, DragEnableKey) boolValue]) {
        return;
    }
    
    UITouch *touch = [touches anyObject];
    
    CGPoint nowPoint = [touch locationInView:self];
    
    float offsetX = nowPoint.x - beginPoint.x;
    float offsetY = nowPoint.y - beginPoint.y;
    
    CGPoint point = CGPointMake(self.center.x + offsetX, self.center.y + offsetY);
    [self.dragDelegate dragMoveButtonWithTouchPoint:point];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.highlighted) {
        [self sendActionsForControlEvents:UIControlEventTouchUpInside];
        self.highlighted = NO;
    }
    
    if (self.superview && [objc_getAssociatedObject(self,AdsorbEnableKey) boolValue] ) {
        [UIView animateWithDuration:0.125 animations:^(void){
            
            
                self.frame = CGRectMake(originPoint.x,
                                        originPoint.y,
                                        self.frame.size.width,
                                        self.frame.size.height);
            
        }];
        CGPoint touchedEndedPoint = [[touches anyObject]locationInView:self.superview];
        CGFloat touchOffset = sqrtf((touchedEndedPoint.x-originPoint.x)*(touchedEndedPoint.x - originPoint.x)+(touchedEndedPoint.y - originPoint.y)*(touchedEndedPoint.y - originPoint.y));
        
        [self.dragDelegate cancelDragButton];

        if (touchOffset > 40.00) {

            [self.dragDelegate didDragButtonWithPoint:touchedEndedPoint];
        }
        
    
    }
}


@end
