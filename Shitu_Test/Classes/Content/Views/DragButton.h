//
//  UIButton+NMCategory.h
//  DragButtonDemo
//
//  Created by Aster0id on 14-5-16.
//
//

#import <UIKit/UIKit.h>

@protocol ButtonDidDragDelegate <NSObject>
-(void)beginDragButtonWithTouchPoint:(CGPoint)touchPoint;
-(void)dragMoveButtonWithTouchPoint:(CGPoint)touchPoint;
-(void)didDragButtonWithPoint:(CGPoint)point;
-(void)cancelDragButton;
@end

@interface DragButton :UIButton

@property(nonatomic,assign,getter = isDragEnable)   BOOL dragEnable;
@property(nonatomic,assign,getter = isAdsorbEnable) BOOL adsorbEnable;
@property(nonatomic,assign)id<ButtonDidDragDelegate> dragDelegate;
@end
