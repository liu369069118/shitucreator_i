//
//  RecordView.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/7.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordView : UIView
@property(nonatomic,strong)UIButton *drawBtn;
@property(nonatomic,strong)UIButton *recordBtn;
@property(nonatomic,strong)UIButton *cleanBtn;

@end
