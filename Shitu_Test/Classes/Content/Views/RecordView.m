//
//  RecordView.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/7.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "RecordView.h"

@implementation RecordView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.drawBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _drawBtn.frame  = CGRectMake(0, 0, Shitu_SCREENWIDTH/3, self.frame.size.height);
    [_drawBtn setTitle:@"画线" forState:UIControlStateNormal];
    [self addSubview:_drawBtn];
    
    self.recordBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _recordBtn.frame = CGRectMake(Shitu_SCREENWIDTH/3, 0, Shitu_SCREENWIDTH/3, self.frame.size.height);
    [_recordBtn setTitle:@"记录" forState:UIControlStateNormal];
    [self addSubview:_recordBtn];
    
    self.cleanBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _cleanBtn.frame = CGRectMake(Shitu_SCREENWIDTH*2/3, 0, Shitu_SCREENWIDTH/3, self.frame.size.height);
    [_cleanBtn setTitle:@"清除" forState:UIControlStateNormal];
    [self addSubview:_cleanBtn];
    
}


@end
