//
//  ScaleViewController.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/12.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI/BMapKit.h>

@protocol  MeasurigScaleDelegate<NSObject>

-(void)changeDintanceAtMap;

@end

@interface ScaleViewController : UIViewController<BMKMapViewDelegate,MeasurigScaleDelegate>
@property(nonatomic,strong)UIImageView *picImageView;
@property (nonatomic,strong )BMKMapView *mapView;
@property(nonatomic,assign)id<MeasurigScaleDelegate>measuringScaleDelegate;
@property(nonatomic,copy)NSString *imageFilePath;
@property(nonatomic,copy)NSString *area;
@property(nonatomic,copy)NSString *storeID;
@property(nonatomic,copy)NSString *imageName;
@property(nonatomic,copy)NSString *floorName;
@property(nonatomic,assign)float lat;
@property(nonatomic,assign)float lng;
@end
