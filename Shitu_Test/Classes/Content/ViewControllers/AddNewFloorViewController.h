//
//  AddNewFloorViewController.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/18.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewFloorViewController : UIViewController
@property(nonatomic,copy)NSString *storeName;
@property(nonatomic,assign)int storeID;
@property(nonatomic,assign)float lat;
@property(nonatomic,assign)float lng;
@end
