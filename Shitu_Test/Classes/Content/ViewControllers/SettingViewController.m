//
//  SettingViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/7/8.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "SettingViewController.h"
#import "LoginViewController.h"
#import "MallInfoManager.h"
#import "DisclaimerViewController.h"
#import "AboutViewController.h"
@interface SettingViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableView;
@end


@implementation SettingViewController
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.alpha = 1.0;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"设置";
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Shitu_SCREENWIDTH, Shitu_SCREENHEIGHT) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.bounces = NO;
    
    self.tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Shitu_SCREENWIDTH, 348/2)];
    UIImageView *icon = [[UIImageView alloc]initWithFrame:CGRectMake((Shitu_SCREENWIDTH-216/2)/2,(self.tableView.tableHeaderView.bounds.size.height-218/2)/2, 216/2, 218/2)];
    icon.image = [UIImage imageNamed:@"login_logo"];
    [self.tableView.tableHeaderView addSubview:icon];
    
    [self.view addSubview:self.tableView];
    
    // Do any additional setup after loading the view.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 1;
            break;
        default:
            break;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"settingCell";
    
    UITableViewCell *cell = [self.tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
    }
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"免责条款";
                    cell.textLabel.textColor = [UIColor blackColor];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.showsReorderControl = YES;
                    break;
                case 1:
                    cell.textLabel.text = @"关于";
                    cell.textLabel.textColor = [UIColor blackColor];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.showsReorderControl = YES;
                    break;
                default:
                    break;
            }
           
            break;
        case 1:
            cell.textLabel.text = @"退出账号";
            cell.textLabel.textColor = [UIColor redColor];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        default:
            break;
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    [self disclaimer];
                    break;
                case 1:
                    [self about];
                    break;
                    
                default:
                    break;
            }
            
            break;
        case 1:
            [self logOut];
        default:
            break;
    }
     [tableView deselectRowAtIndexPath:indexPath animated:NO]; 
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
////    return 348/2;
//}
-(void)disclaimer
{
    DisclaimerViewController *DisclaimerVc = [[DisclaimerViewController alloc]init];
    [self.navigationController pushViewController:DisclaimerVc animated:YES];

}
-(void)about
{
    AboutViewController *aboutVc = [[AboutViewController alloc]init];
    [self.navigationController pushViewController:aboutVc animated:YES];
}
-(void)logOut
{
    LoginViewController *logInVc = [[LoginViewController alloc]init];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"token"];
    NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
    [[NSFileManager defaultManager]removeItemAtPath:[[MallInfoManager shareInstance]filePathWithName:[NSString stringWithFormat:@"%@storeInfo" ,userName]] error:nil];
    [self.navigationController pushViewController:logInVc animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
