//
//  AddNewFloorViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/18.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "AddNewFloorViewController.h"
#import "ScaleViewController.h"
@interface AddNewFloorViewController ()<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)UIButton *takePhoto;
@property(nonatomic,strong)UIButton *photoAlbum;
@property(nonatomic,strong)UITextField *floorDetail;
@property(nonatomic,copy)NSString *floorName;
@property(strong,nonatomic)NSArray *leftpickerData;
@property(nonatomic,strong)NSArray *rightpickerData;
@property(nonatomic,assign)int area;
@property(nonatomic,assign)int areaLeft;
@property(nonatomic,assign)int areaRight;
@end

@implementation AddNewFloorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"添加楼层";
    self.navigationItem.backBarButtonItem.title = @"";
    self.view.backgroundColor = Shitu_ColorWithRgbValue(0xefeff4);
    
    _area = 0;
    _floorName = @"F1";
    UILabel *storeNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 64, Shitu_SCREENWIDTH, 66)];
    storeNameLabel.font = [UIFont systemFontOfSize:20.0];
    storeNameLabel.backgroundColor = [UIColor whiteColor];
    storeNameLabel.textAlignment = NSTextAlignmentCenter;
    storeNameLabel.text = _storeName;
    [self.view addSubview:storeNameLabel];
    
    UIImageView *storeIcon = [[UIImageView alloc]initWithFrame:CGRectMake(20, 64+23, 30, 30)];
    storeIcon.backgroundColor = [UIColor whiteColor];
    storeIcon.image = [UIImage imageNamed:@"add_floor_mall_icon"];
    [self.view addSubview:storeIcon];
    
    UILabel *floorNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, (64+66+215)*Shitu_SCREENHEIGHT/667, 100,43)];
    floorNameLabel.backgroundColor = [UIColor whiteColor];
    floorNameLabel.text = @"楼层名称：";
    floorNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:floorNameLabel];
    
    _floorDetail = [[UITextField alloc]initWithFrame:CGRectMake(100,(64+66+215)*Shitu_SCREENHEIGHT/667, Shitu_SCREENWIDTH-100,43)];
    _floorDetail.selected = YES;
    _floorDetail.delegate = self;
    _floorDetail.placeholder = @"请输入楼层别名(可选)";
    _floorDetail.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_floorDetail];
    
    _takePhoto = [UIButton buttonWithType:UIButtonTypeSystem];
    _takePhoto.frame = CGRectMake(0, (64+66+215+43+110)*Shitu_SCREENHEIGHT/667, Shitu_SCREENWIDTH, 43);
    _takePhoto.backgroundColor = [UIColor whiteColor];
    [_takePhoto setTitle:@"拍照" forState:UIControlStateNormal];
    _takePhoto.titleLabel.font = [UIFont systemFontOfSize:16];
    _takePhoto.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_takePhoto addTarget:self action:@selector(takePhoto:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_takePhoto];
    
    _photoAlbum = [UIButton buttonWithType:UIButtonTypeSystem];
    _photoAlbum.frame = CGRectMake(0, (64+66+215+43+110+43)*Shitu_SCREENHEIGHT/667, Shitu_SCREENWIDTH, 43);
    _photoAlbum.backgroundColor = [UIColor whiteColor];
    [_photoAlbum setTitle:@"从相册中添加" forState:UIControlStateNormal];
    _photoAlbum.titleLabel.font = [UIFont systemFontOfSize:16];
    _photoAlbum.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_photoAlbum addTarget:self action:@selector(selectPhotoFromAlbum:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_photoAlbum];
    
    UIPickerView *pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, (66+64)*Shitu_SCREENHEIGHT/667, Shitu_SCREENWIDTH, 215*Shitu_SCREENHEIGHT/667)];
    pickerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pickerView];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    _leftpickerData = [NSArray arrayWithObjects:@"F",@"B", nil];
    
    // Do any additional setup after loading the view.
}
-(void)takePhoto:(UIButton *)sender
{
    NSLog(@"拍照");
    //先设定sourceType为相机，然后判断相机是否可用（ipod没相机），不可用将sourceType设定为相片库
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];//初始化
    picker.delegate = self;

//    picker.allowsEditing = YES;//设置可编辑
    picker.sourceType = sourceType;
    [self presentViewController:picker animated:YES completion:nil];//进入照相界面
    
}
-(void)selectPhotoFromAlbum:(UIButton *)sender
{
    NSLog(@"从照片添加");
    UIImagePickerController *pickerImage = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        pickerImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        pickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        pickerImage.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:pickerImage.sourceType];
        
    }
    pickerImage.delegate = self;
    pickerImage.allowsEditing = NO;
    [self presentViewController:pickerImage animated:YES completion:nil];
}
#pragma mark imagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
        UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imageSize = image.size;
        imageSize.width = 400;
        imageSize.height = 400*image.size.height/image.size.width;
        image = [self imageWithImage:image scaledToSize:imageSize];
        
        NSData *data;
        
        if (UIImagePNGRepresentation(image) == nil)
        {
            data = UIImageJPEGRepresentation(image, 1.0);
        }
        else
        {
            data = UIImagePNGRepresentation(image);
        }
        
        //图片保存的路径
        //这里将图片放在沙盒的cache文件夹中
        NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *path=[paths objectAtIndex:0];
        
        //文件管理器
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        //把刚刚图片转换的data对象拷贝至沙盒中 并保存为image.png
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
        [fileManager createFileAtPath:[path stringByAppendingString:[NSString stringWithFormat:@"/%@.png",_floorDetail.text]] contents:data attributes:nil];
        
        //得到选择后沙盒中图片的完整路径
       NSString *filePath = [NSString stringWithFormat:@"%@/%@.png",path,_floorDetail.text];
        
        //关闭相册界面
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        ScaleViewController *scaleVc = [[ScaleViewController alloc]init];
        scaleVc.imageFilePath = filePath;
        NSLog(@"area = %d",_area);
        scaleVc.floorName = _floorName;
        scaleVc.imageName = [NSString stringWithFormat:@"%d%d",_storeID,_area];
        scaleVc.storeID =[NSString stringWithFormat:@"%d", _storeID];
        scaleVc.area =[NSString stringWithFormat:@"%d", _area];
        scaleVc.lat = self.lat;
        scaleVc.lng = self.lng;
        [self.navigationController pushViewController:scaleVc animated:YES];
    }
}
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark PickDelegate
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return 2;
        case 1:
            return 1;
        case 2:
            return 30;
        case 3:
            return 1;
        default:
            return 0;
    }
    
}
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 50;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 4;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return _leftpickerData[row];
            break;
        case 1:
            return @"层";
        case 2:
            return [NSString stringWithFormat:@"%ld",(long)row+1];
        case 3:
            return @"层";
        default:
            break;
    }
    return nil;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            _areaLeft = (int)row;
            break;
        case 2:
            _areaRight = (int)(row+1);
            break;
        default:
            break;
    }
    if (_areaLeft==0) {
        _area = _areaRight;
        _floorName = [NSString stringWithFormat:@"F%d",_areaRight];
    }
    else if (_areaLeft==1)
    {
        _area = -_areaRight+1;
        _floorName = [NSString stringWithFormat:@"B%d",_areaRight];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
        UITouch *touch = [touches anyObject];
        if ((touch.tapCount == 1)&&[_floorDetail isFirstResponder]) {
            
        [_floorDetail resignFirstResponder];
        }

    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([_floorDetail isFirstResponder]) {
        [_floorDetail resignFirstResponder];
    }
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
