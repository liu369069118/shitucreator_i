//
//  ContentViewController.h
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI/BMapKit.h>
@interface ContentViewController : UIViewController<UIAlertViewDelegate,BMKMapViewDelegate,BMKLocationServiceDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong )BMKMapView *mapView;
@property(nonatomic,strong)UIButton *startBtn;
@property(nonatomic,strong)BMKLocationService *locService;
-(void)startLocation:(id)sender;
@end
