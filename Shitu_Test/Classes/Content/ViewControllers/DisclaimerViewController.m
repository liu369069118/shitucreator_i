//
//  DisclaimerViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/7/8.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "DisclaimerViewController.h"

@interface DisclaimerViewController ()
@property(nonatomic,strong)UIWebView *webView;
@property(retain,nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation DisclaimerViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"免责条款";
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, Shitu_SCREENWIDTH, Shitu_SCREENHEIGHT)];
     _webView.scrollView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    _webView.delegate = self;

    _webView.scalesPageToFit =YES;
    self.activityIndicatorView = [[UIActivityIndicatorView alloc]initWithFrame: CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [self.activityIndicatorView setCenter: self.view.center] ;
    [self.activityIndicatorView setActivityIndicatorViewStyle: UIActivityIndicatorViewStyleWhite] ;
    [self.view addSubview : self.activityIndicatorView];
    [self.view addSubview:_webView];
    
    [self loadWebPageWithString];
    // Do any additional setup after loading the view.
}

- (void)loadWebPageWithString
{
    NSURL *url =[NSURL URLWithString:Disclaimer];
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activityIndicatorView startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicatorView stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertView *alterview = [[UIAlertView alloc] initWithTitle:@"加载失败。" message:[error localizedDescription]  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alterview show];
    alterview = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
