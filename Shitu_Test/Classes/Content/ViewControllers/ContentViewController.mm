//
//  ContentViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//
#import "ContentViewController.h"
#import "LoginViewController.h"
#import "MallInfoManager.h"
#import "StoreModel.h"
#import "FloorModel.h"
#import "JSONKit.h"
#import "ConciseKit.h"
#import "AddStoreViewController.h"
#import "DragButton.h"
#import "MyAnimatedAnnotationView.h"
#import "AddNewFloorViewController.h"
#import "MBProgressHUD.h"
#import "SDWebImageDownloader.h"
#import "NSString+URL.h"
#import "AccountTokenManager.h"
#import "SettingViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "CalibrateViewController.h"
@interface ContentViewController ()<UIGestureRecognizerDelegate,ButtonDidDragDelegate,SaveNewStoreDelegate,storeJsonWriteDelegate,loginStateDelegate,UIScrollViewDelegate,MBProgressHUDDelegate,UISearchBarDelegate,UIAlertViewDelegate>

@property(nonatomic,strong)UIScrollView *storeView;
@property(nonatomic,strong)UITableView *floorView;
@property(strong,nonatomic)NSMutableArray *stores;
@property(strong,nonatomic)StoreModel *currentStore;
@property(nonatomic,strong)BMKGroundOverlay *overlay;
@property(nonatomic,strong)BMKPointAnnotation *pointAnnotation;
@property(nonatomic,assign)CLLocationCoordinate2D currentSelectCoordinate;
@property(nonatomic,strong) DragButton *dragBtn;
@property(nonatomic,strong)UILabel *remind;
@property(nonatomic,strong)UIImageView *addMall;
@property(nonatomic,assign)BOOL floorsViewState;
@property(nonatomic,assign)BOOL isBubbleSelect;
@property(nonatomic,strong)NSMutableArray *annotations;
@property(nonatomic,strong)UIView *floorBaseView;
//@property(strong)UISearchBar *searchBar;
@end

@implementation ContentViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mapView viewWillAppear];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBarHidden = NO;
    _locService.delegate = self;
    _mapView.delegate = self;
    _mapView.overlookEnabled = NO;
    _floorsViewState = NO;
    _isBubbleSelect = NO;
    [_floorBaseView removeFromSuperview];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_mapView viewWillDisappear];
    _mapView.alpha = 1.0;
    _mapView.delegate = nil;
    _locService.delegate = nil;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getStores];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8) {
        //由于IOS8中定位的授权机制改变 需要进行手动授权
        CLLocationManager *locationManager = [[CLLocationManager alloc] init];
        //获取授权认证
        [locationManager requestAlwaysAuthorization];
        [locationManager requestWhenInUseAuthorization];
    }
    
    
    //    登录判断
    [self logInJudge];
    [AccountTokenManager shareAccountTokenManager].loginStateDelegate = self;
    UIBarButtonItem *setting = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"nav_setting"] style:UIBarButtonItemStylePlain target:self action:@selector(setting:)];
    self.navigationItem.rightBarButtonItem = setting;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    NSLog(@"token = %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]);
    self.navigationItem.title  = @"识途";
    _mapView = [[BMKMapView alloc]initWithFrame:CGRectMake(0, 0, Shitu_SCREENWIDTH, Shitu_SCREENHEIGHT)];
    [_mapView setZoomLevel:17];
    _mapView.showMapScaleBar = YES;
    _mapView.showsUserLocation = YES;
    self.view.backgroundColor = [UIColor blackColor];
    [_mapView setCompassPosition:CGPointMake(100, 100)];
    _mapView.mapScaleBarPosition = CGPointMake(_mapView.frame.size.width - 70, _mapView.frame.size.height - 40);
    [self.view addSubview: _mapView];
    [self addCustomGestures];
    
    
    self.stores = [NSMutableArray array];
    
    _dragBtn = [[DragButton alloc] initWithFrame:CGRectMake(8*Shitu_SCREENWIDTH/375, 534*Shitu_SCREENHEIGHT/667, 80, 80)];
    [_dragBtn setBackgroundImage:[UIImage imageNamed:@"add_mall_btn_drawable"] forState:UIControlStateNormal];
    [_dragBtn setBackgroundImage:[UIImage imageNamed:@"add_mall_btn_cancel"] forState:UIControlStateHighlighted];
    [_dragBtn setDragDelegate:self];
    _dragBtn.tag = 599;
    _dragBtn.layer.cornerRadius = 30;
    [_dragBtn setDragEnable:YES];
    [_dragBtn setAdsorbEnable:YES];
    [self.view insertSubview:_dragBtn aboveSubview:_mapView];
    
    _locService = [[BMKLocationService alloc]init];
    
    
    _startBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _startBtn.frame = CGRectMake(Shitu_SCREENWIDTH*3/4+20, Shitu_SCREENHEIGHT*6/7, 50, 50);
    [_startBtn setBackgroundImage:[UIImage imageNamed:@"home_positioning"] forState:UIControlStateNormal];
    [_startBtn addTarget:self action:@selector(startLocation:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_startBtn];
    
    [MallInfoManager shareInstance].storeDelegate = self;
    [self startLocation:nil];
    
    _isBubbleSelect = NO;
    //poi搜索
//    [self initPoiSearch];
//    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 64, Shitu_SCREENWIDTH, 40)];
//    [self.view addSubview:_searchBar];
    // Do any additional setup after loading the view.
}
-(void)logInJudge
{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"token"]&&[[[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] objectForKey:@"auth"]isEqualToString:@"ok"]) {
        
        NSString *token = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"token"];
        [[AccountTokenManager shareAccountTokenManager]authWithLoginURL:VERIFYTOKEN authData:[NSMutableDictionary dictionaryWithDictionary:@{@"t":token}]];
        
        
    }else{
        LoginViewController *logVc = [[LoginViewController alloc]init];
        NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
        [[NSFileManager defaultManager]removeItemAtPath:[[MallInfoManager shareInstance]filePathWithName:[NSString stringWithFormat:@"%@storeInfo" ,userName]] error:nil];
        [self.navigationController pushViewController:logVc animated:YES];
    }
    
}
-(void)authSuccessAndlogin
{
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"authState"]isEqualToString:@"ok"]) {
        [self getStores];
    }
    else
    {
        LoginViewController *logVc = [[LoginViewController alloc]init];
        NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
        [[NSFileManager defaultManager]removeItemAtPath:[[MallInfoManager shareInstance]filePathWithName:[NSString stringWithFormat:@"%@storeInfo" ,userName]] error:nil];
        [self.navigationController pushViewController:logVc animated:YES];
    }
}
-(void)authFailed
{
    LoginViewController *logVc = [[LoginViewController alloc]init];
    NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
    [[NSFileManager defaultManager]removeItemAtPath:[[MallInfoManager shareInstance]filePathWithName:[NSString stringWithFormat:@"%@storeInfo" ,userName]] error:nil];
    [self.navigationController pushViewController:logVc animated:YES];
}

//注销
-(void)setting:(id)sender
{
    SettingViewController *setVc = [[SettingViewController alloc]init];
    [self.navigationController pushViewController:setVc animated:YES];
}
-(void)addAllAnnotation
{
    _annotations = [NSMutableArray array];
    for (int i = 0; i<_stores.count; i++) {
        StoreModel *store = _stores[i];
        CLLocationCoordinate2D coor;
        coor.latitude = store.lat;
        coor.longitude = store.lng;
        BMKPointAnnotation *pointAnnitation = [[BMKPointAnnotation alloc]init];
        pointAnnitation.coordinate = coor;
        pointAnnitation.title = store.name;
        pointAnnitation.subtitle = nil;
        
        [_annotations addObject:pointAnnitation];
        
    }
    [_mapView addAnnotations:_annotations];
}

-(void)SavaNewStoreWithCoor:(CLLocationCoordinate2D)coor
{
    [self getStores];
}

#pragma mark 松手添加商场

- (void)addPointAnnotationWithLocation:(CLLocationCoordinate2D )locationCoordinate
{
    AddStoreViewController *storeVc = [[AddStoreViewController alloc]init];
    storeVc.addedStoreCoor = locationCoordinate;
    storeVc.saveDelegate = self;
    [self.navigationController pushViewController:storeVc animated:YES];
    
}

//普通态
-(void)startLocation:(id)sender
{
    //
    NSLog(@"进入普通定位态");
    [_locService startUserLocationService];
    
    _mapView.showsUserLocation = NO;//先关闭显示的定位图层
    _mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
    _mapView.showsUserLocation = YES;//显示定位图层
    _startBtn.alpha = 0.6;
    
}

//停止定位
-(void)stopLocation:(id)sender
{
    [_locService stopUserLocationService];
    _mapView.showsUserLocation = NO;
    
    [_startBtn setEnabled:YES];
    [_startBtn setAlpha:1.0];
}

/**
 *在地图View将要启动定位时，会调用此函数
 *@param mapView 地图View
 */
- (void)willStartLocatingUser
{
    NSLog(@"start locate");
}

/**
 *用户方向更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation
{
    [_mapView updateLocationData:userLocation];
    NSLog(@"heading is %@",userLocation.heading);
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    NSLog(@"didUpdateUserLocation lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    BMKCoordinateRegion region;
    region.center = location;
    region.span.latitudeDelta = 0.1;
    region.span.longitudeDelta = 0.1;
    
    BMKCoordinateRegion adjustedRegion = [_mapView regionThatFits:region];
    [_mapView setRegion:adjustedRegion animated:YES];
    [_mapView updateLocationData:userLocation];
    [_locService stopUserLocationService];
    
}
/**
 *在地图View停止定位后，会调用此函数
 *@param mapView 地图View
 */
- (void)didStopLocatingUser
{
    NSLog(@"stop locate");
}

/**
 *定位失败后，会调用此函数
 *@param mapView 地图View
 *@param error 错误号，参考CLError.h中定义的错误号
 */
- (void)didFailToLocateUserWithError:(NSError *)error
{
    NSLog(@"location error");
}

#pragma mark -getStoreInfoDelegate
-(void)getStores
{
    
    NSString *token = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"token"];
    [[MallInfoManager shareInstance]WriteMallInfoToSandboxWithStoreURL:STOREINFO tokenData:[NSMutableDictionary dictionaryWithDictionary:@{@"t":token}]];
    
}
-(void)getStoreInfoFromServer:(NSMutableDictionary *)storeInfo
{
    [_stores removeAllObjects];
    if ([[storeInfo objectForKey:@"auth"]isEqualToString:@"ok"]) {
        
        NSArray *json = [NSArray arrayWithArray:[storeInfo objectForKey:@"json"]];
        for (NSDictionary *storeDic in json) {
            //            NSLog(@"json = %@",json);
            StoreModel  *storeModel = [[StoreModel alloc]initWithAttributes:storeDic];
            NSArray *floors = $safe([storeDic objectForKey:@"floors"]);
            storeModel.floors = [NSMutableArray array];
            for (NSDictionary *floor in floors) {
                
                FloorModel *floorModel = [[FloorModel alloc]initWithAttributes:floor];
                [storeModel.floors addObject:floorModel];
                floorModel = nil;
            }
            [_stores addObject:storeModel];
            storeModel = nil;
        }
    }
    [_mapView removeAnnotations:_annotations];
    [self addAllAnnotation];
    
}

-(void)getStoreInfoFailed:(NSError *)error
{
    NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
    NSString *path = [[MallInfoManager shareInstance]filePathWithName:[NSString stringWithFormat:@"%@storeInfo",userName]];
    
    BOOL fileExist =[[NSFileManager defaultManager]fileExistsAtPath:path];
    if (!fileExist) {
        
        NSLog(@"请求商户信息超时！%p",&error);
    }
    else
    {
        [[MallInfoManager shareInstance]getStoreJsonFromSandboxWithPathName:path];
        if ([[MallInfoManager shareInstance]getStoreInfoForStores:_stores]) {
            [self addAllAnnotation];
        }
    }
    
}
#pragma mark -uiscrollViewDelegate
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    int i = 0;
//    if (self.storeView.contentOffset.x < -Shitu_SCREENWIDTH*2/3) {
//        i++;
//        _currentStore = _stores[i];
//    }
//    if (self.storeView.contentOffset.x > Shitu_SCREENWIDTH*2/3) {
//        if (i==0) {
//            return;
//        }
//        i--;
//        _currentStore = _stores[i];
//    }
//}
#pragma mark -UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _currentStore.floors.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdetify = @"floorTableViewCell";
    UITableViewCell *cell = [_floorView dequeueReusableCellWithIdentifier:reuseIdetify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdetify];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        cell.showsReorderControl = YES;
        
    }
    cell.textLabel.backgroundColor = [UIColor clearColor];
    FloorModel *floor = _currentStore.floors[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",floor.floor_name];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FloorModel *floor = _currentStore.floors[indexPath.row];
    [self writeImageToSandbox:floor index:(int)indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
-(void)addNewFloor:(UIButton *)sender
{
    AddNewFloorViewController *newFloorVc = [[AddNewFloorViewController alloc]init];
    newFloorVc.storeName = _currentStore.name;
    newFloorVc.storeID = (int)_currentStore.storeID;
    newFloorVc.lat = _currentStore.lat;
    newFloorVc.lng = _currentStore.lng;
    [self.navigationController pushViewController:newFloorVc animated:YES];
}
-(void )writeImageToSandbox:(FloorModel *)floor index:(int )index
{
    
    NSString *pathName = [floor.map_url URLEncodedString];
    NSString *path = [[pathName substringFromIndex:40]componentsSeparatedByString:@"."][0];
    NSString *image_path=[self filePathWithName:path];
    
    
    CalibrateViewController *calibrateVc = [[CalibrateViewController alloc]init];
    calibrateVc.mapURL = floor.map_url;
    calibrateVc.whichArea = floor.floor;
    calibrateVc.storeID = _currentStore.storeID;
    calibrateVc.currentStore = _currentStore;
    calibrateVc.floorIndex = index;
    if ([[NSFileManager defaultManager]fileExistsAtPath:image_path]) {
        calibrateVc.path = image_path;
        [self.navigationController pushViewController:calibrateVc animated:YES];
    }
    else{
    NSURL *URL = [NSURL URLWithString:[floor.map_url URLEncodedString]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    //下载请求
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:image_path append:YES];
    //下载进度回调
    
    __block MBProgressHUD *hud = [[MBProgressHUD alloc]initWithView:self.view];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        //下载进度
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view addSubview:hud];
            hud.labelText = @"努力加载中...";
            
            //设置模式为进度框形的
            hud.mode = MBProgressHUDModeDeterminate;
            hud.progress = totalBytesRead*1.0/totalBytesExpectedToRead;
            [hud show:YES];
        });
        
    }];
    //成功和失败回调
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        dispatch_async(dispatch_get_main_queue(), ^{

            calibrateVc.path = image_path;
            [hud hide:YES];
            [self.navigationController pushViewController:calibrateVc animated:YES];
        });

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hide:YES];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"加载失败！" message:@"请检查网络连接" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
            alertView = nil;
        });

    }];
    [operation start];
    }
}
//json命名
-(NSString *)filePathWithName:(NSString *)name
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path=[paths objectAtIndex:0];
    NSString *imagePath=[path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    return imagePath;
}

#pragma mark - BMKMapViewDelegate

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    
}

- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate {
    NSLog(@"map view: click blank");
    _isBubbleSelect = NO;
    if (_floorsViewState == YES) {
        [_floorBaseView removeFromSuperview];
        _mapView.alpha = 1.0;
    }
    _floorsViewState = NO;
    
}

- (void)mapview:(BMKMapView *)mapView onDoubleClick:(CLLocationCoordinate2D)coordinate {
    NSLog(@"map view: double click");
}
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    //普通annotation
    if (annotation == _pointAnnotation) {
        NSString *AnnotationViewID = @"renameMark";
        BMKPinAnnotationView *annotationView = (BMKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        if (annotationView == nil) {
            annotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
            // 设置颜色
            annotationView.pinColor = BMKPinAnnotationColorPurple;
            // 从天上掉下效果
            annotationView.animatesDrop = NO;
            // 设置可拖拽
            annotationView.draggable = YES;
        }
        return annotationView;
    }
    return nil;
}

// 当点击annotation view弹出的泡泡时，调用此接口
- (void)mapView:(BMKMapView *)mapView annotationViewForBubble:(BMKAnnotationView *)view;
{
    if (!_floorsViewState) {
        
        for (StoreModel *store in _stores) {
            if ((store.lat == _currentSelectCoordinate.latitude)&&(store.lng == _currentSelectCoordinate.longitude)) {
                _currentStore = store;
            }
        }
        _floorBaseView = [[UIView alloc]initWithFrame:CGRectMake((110/2)*Shitu_SCREENWIDTH/375, (306/2)*Shitu_SCREENHEIGHT/667, 530/2, 686/2)];
        _floorBaseView.center = self.view.center;
        self.floorView = [[UITableView alloc]initWithFrame:CGRectMake(0, 170/2 ,530/2,416/2) style:UITableViewStylePlain];
        [_floorBaseView addSubview:self.floorView];
        
        UIImageView *headerView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 530/2, 170/2)];
        headerView.image = [UIImage imageNamed:@"content_tableHeader"];
        [_floorBaseView addSubview:headerView];
        
        UILabel *storeName = [[UILabel alloc]initWithFrame:CGRectMake(174/2, 42/2, 530/2-174/2, 22)];
        storeName.backgroundColor = [UIColor clearColor];
        storeName.textColor = [UIColor whiteColor];
        storeName.font = [UIFont systemFontOfSize:21];
        storeName.text = _currentStore.name;
        [headerView addSubview:storeName];
        
        UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 586/2, 530/2, 100/2)];
        footView.backgroundColor = [UIColor whiteColor];
        
        UIButton *addNewFloor = [UIButton buttonWithType:UIButtonTypeCustom];
        [addNewFloor setImage:[UIImage imageNamed:@"btn_add_floor_selected"] forState:UIControlStateNormal];
        addNewFloor.frame = CGRectMake(228/2, 7, 35, 35);
        [addNewFloor addTarget:self action:@selector(addNewFloor:) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:addNewFloor];
        
        [_floorBaseView addSubview:footView];
        
        _floorView.delegate = self;
        _floorView.dataSource = self;
        
        [self.view insertSubview:_floorBaseView aboveSubview:self.mapView];
        _mapView.alpha = 0.5;
        _floorsViewState = YES;
        
    }
    
}

-(void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view
{
    if (view.selected) {
        _isBubbleSelect = YES;
        _currentSelectCoordinate = view.annotation.coordinate;
        for (StoreModel *store in _stores) {
            if ((store.lat == _currentSelectCoordinate.latitude)&&(store.lng == _currentSelectCoordinate.longitude)) {
                _currentStore = store;
            }
        }
    }
    else{
        _isBubbleSelect = NO;
    }
}

#pragma mark 添加自定义手势
- (void)addCustomGestures {
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTap.delegate = self;
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.cancelsTouchesInView = NO;
    doubleTap.delaysTouchesEnded = NO;
    
    [self.view addGestureRecognizer:doubleTap];
     
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.delegate = self;
    singleTap.cancelsTouchesInView = NO;
    singleTap.delaysTouchesEnded = NO;
    [singleTap requireGestureRecognizerToFail:doubleTap];
    [self.view addGestureRecognizer:singleTap];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)theSingleTap {
    /*
     *do something
     */
    NSLog(@"my handleSingleTap");
    
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)theDoubleTap {
    /*
     *do something
     */
    NSLog(@"my handleDoubleTap");
}
#pragma mark buttonDragDelegate
-(void)beginDragButtonWithTouchPoint:(CGPoint)touchPoint
{
    _remind = [[UILabel alloc]initWithFrame:CGRectMake(0, Shitu_SCREENHEIGHT-50, Shitu_SCREENWIDTH, 50)];
    _remind.text = @"松手添加楼层";
    _remind.textAlignment = NSTextAlignmentCenter;
    _remind.backgroundColor = [UIColor blackColor];
    _remind.textColor = [UIColor whiteColor];
    _remind.alpha = 0.7;
    
    UIImageView *floorImage = [[UIImageView alloc]initWithFrame:CGRectMake(121, 10, 30, 30)];
    floorImage.image = [UIImage imageNamed:@"home_louceng"];
    [_remind addSubview:floorImage];
    [self.view addSubview:_remind];
    
    _addMall = [[UIImageView alloc]initWithFrame:CGRectMake(self.dragBtn.frame.origin.x + self.dragBtn.frame.size.width, self.dragBtn.frame.origin.y - 40, 130, 85)];
    _addMall.image = [UIImage imageNamed:@"mark_add_mall"];
    [self.view addSubview:_addMall];
}

-(void)dragMoveButtonWithTouchPoint:(CGPoint)touchPoint
{
    _addMall.frame = CGRectMake(touchPoint.x+self.dragBtn.frame.size.width/2, touchPoint.y - self.dragBtn.frame.size.height/2 -40, self.addMall.frame.size.width, self.addMall.frame.size.height);
    
    if (_addMall.center.y+_addMall.bounds.size.height/2>Shitu_SCREENHEIGHT-50) {
        
        _addMall.image = [UIImage imageNamed:@"mark_add_floor"];
    }
    else if(CGRectContainsPoint(_dragBtn.frame, CGPointMake( _addMall.center.x-_addMall.bounds.size.width/2, _addMall.center.y+_addMall.bounds.size.height/2)))
    {
        _addMall.image = [UIImage imageNamed:@"mark_add_floor_cancel"];
    }
    else
    {
        _addMall.image = [UIImage imageNamed:@"mark_add_mall"];
    }
}
-(void)cancelDragButton
{
    [_remind removeFromSuperview];
    [_addMall removeFromSuperview];
    [self reloadInputViews];
}
-(void)didDragButtonWithPoint:(CGPoint)point
{
    if (_addMall.center.y+_addMall.bounds.size.height/2>Shitu_SCREENHEIGHT-50) {
        if (_isBubbleSelect) {
            
            AddNewFloorViewController *newFloorVc = [[AddNewFloorViewController alloc]init];
            newFloorVc.storeName = _currentStore.name;
            newFloorVc.storeID = (int)_currentStore.storeID;
            newFloorVc.lat = _currentStore.lat;
            newFloorVc.lng = _currentStore.lng;
            [self.navigationController pushViewController:newFloorVc animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请选择信息点" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            alert = nil;
        }
        
    }
    else{
        CLLocationCoordinate2D coor = [_mapView convertPoint:CGPointMake(_addMall.center.x, _addMall.center.y+_addMall.bounds.size.height/2) toCoordinateFromView:self.view];
        [self addPointAnnotationWithLocation:coor];
    }
    
}

//#pragma mark poi搜索
//-(void)initPoiSearch
//{
//    _poiSearch = [[BMKPoiSearch alloc]init];
//    _poiSearch.delegate = self;
//    currentPage = 0;
//    //附近云检索，其他检索方式见详细api
//    BMKNearbySearchOption *nearBySearchOption = [[BMKNearbySearchOption alloc]init];
//    nearBySearchOption.pageIndex = currentPage; //第几页
//    nearBySearchOption.pageCapacity = 10;  //最多几页
//    nearBySearchOption.keyword = _searchBar.text;   //检索关键字
//    nearBySearchOption.location = [UserLocationManager sharedInstance].clloction.coordinate; // poi检索点
//    nearBySearchOption.radius = 1000; //检索范围 m
//    BOOL flag = [_poiSearch poiSearchNearBy:nearBySearchOption];
//    if(flag)
//    {
//        NSLog(@"城市内检索发送成功");
//    }
//    else
//    {
//        NSLog(@"城市内检索发送失败");
//    }
//    
//}
//#pragma mark --BMKPoiSearchDelegate
///**
// *返回POI搜索结果
// *@param searcher 搜索对象
// *@param poiResult 搜索结果列表
// *@param errorCode 错误号，@see BMKSearchErrorCode
// */
//- (void)onGetPoiResult:(BMKPoiSearch*)searcher result:(BMKPoiResult*)poiResult errorCode:(BMKSearchErrorCode)errorCode
//{
//    if (errorCode == BMK_SEARCH_NO_ERROR)
//    {
//        for (int i = 0; i < poiResult.poiInfoList.count; i++)
//        {
//            BMKPoiInfo* poi = [poiResult.poiInfoList objectAtIndex:i];
//            
//        }
//    }
//    
//}
//#pragma mark search bar delegate methods
//
////点击键盘上的search按钮时调用
//- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
//{
//    NSString *searchTerm = searchBar.text;
//    //    [self handleSearchForTerm:searchTerm];
//}
//
////输入文本实时更新时调用
//- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    //    if (searchText.length == 0) {
//    //        [self resetSearch];
//    //        [table reloadData];
//    //        return;
//    //    }
//    //
//    //    [self handleSearchForTerm:searchText];
//}
//
////cancel按钮点击时调用
//- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
//{
//    //    isSearching = NO;
//    //    search.text = @"";
//    //    [self resetSearch];
//    //    [table reloadData];
//    [searchBar resignFirstResponder];
//}
//
////点击搜索框时调用
//- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
//{
//    //    isSearching = YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
