//
//  CorrectViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/7/20.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "CalibrateViewController.h"
#import "openGLViewController.h"
#import "SHTMagCalibrator.h"
@interface CalibrateViewController ()<SHTMagCalibratorDeletegate>
@property(nonatomic,strong)SHTMagCalibrator *magCalibrator;
@property(nonatomic,strong)UILabel *percentLabel;
@property(nonatomic,assign)float percent;
@end

@implementation CalibrateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.magCalibrator = [[SHTMagCalibrator alloc]init];
    self.magCalibrator.delegate = self;
    [self.magCalibrator startCalibration];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"login_BG"]]];
    UIImageView *calibrate = [[UIImageView alloc]initWithFrame:CGRectMake(56*Shitu_SCREENWIDTH/375, 204*Shitu_SCREENHEIGHT/667, 263*Shitu_SCREENWIDTH/375,135*Shitu_SCREENHEIGHT/667)];
    calibrate.image = [UIImage imageNamed:@"calibrate_pic"];
    [self.view addSubview:calibrate];
    
    UILabel *header1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 95*Shitu_SCREENHEIGHT/667, Shitu_SCREENWIDTH, 20)];
    header1.text = @"握住设备";
    header1.textAlignment = NSTextAlignmentCenter;
    header1.tintColor = Shitu_ColorWithRgbValue(0x393939);
    header1.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:header1];
    
    UILabel *header2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 95*Shitu_SCREENHEIGHT/667 + 20, Shitu_SCREENWIDTH, 20)];
    header2.text = @"沿8字转动手腕进行校正";
    header2.textAlignment = NSTextAlignmentCenter;
    header2.tintColor = Shitu_ColorWithRgbValue(0x393939);
    header2.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:header2];
    
    _percentLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 461*Shitu_SCREENHEIGHT/667, Shitu_SCREENWIDTH, 50)];
    _percentLabel.text = [[NSString stringWithFormat:@"%3.0f",0.0]stringByAppendingString:@"%"];
    _percentLabel.textAlignment = NSTextAlignmentCenter;
    _percentLabel.textColor = Shitu_ColorWithRgbValue(0x00baf0);
    _percentLabel.font = [UIFont systemFontOfSize:40];
    [self.view addSubview:_percentLabel];
    
    UIButton *skip = [UIButton buttonWithType:UIButtonTypeSystem];
    skip.frame = CGRectMake(88*Shitu_SCREENWIDTH/375, 551*Shitu_SCREENHEIGHT/667, 200, 38);
    [skip setTitle:@"跳过" forState:UIControlStateNormal];
    [skip setBackgroundImage:[UIImage imageNamed:@"calibrate_skip_unselected"] forState:UIControlStateNormal];
    skip.titleLabel.textAlignment = NSTextAlignmentCenter;
    [skip setTitleColor:Shitu_ColorWithRgbValue(0xfb5555) forState:UIControlStateNormal];
    skip.titleLabel.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:skip];
    
    [skip addTarget:self action:@selector(skipDisclaimer:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *footer = [[UILabel alloc]initWithFrame:CGRectMake(0, 604*Shitu_SCREENHEIGHT/667, Shitu_SCREENWIDTH, 20)];
    footer.text = @"跳过可能会导致精确度的降低,请慎重选择";
    footer.textAlignment = NSTextAlignmentCenter;
    footer.tintColor = Shitu_ColorWithRgbValue(0x393939);
    footer.font = [UIFont systemFontOfSize:11];
    [self.view addSubview:footer];
    // Do any additional setup after loading the view.
}
-(void)skipDisclaimer:(UIButton *)sender
{
    openGLViewController *glkVc = [[openGLViewController alloc]init];
    glkVc.mapURL = self.mapURL;
    glkVc.whichArea = self.whichArea;
    glkVc.storeID = self.storeID;
    glkVc.currentStore = self.currentStore;
    glkVc.floorIndex = self.floorIndex;
    glkVc.path = self.path;
    [self.navigationController pushViewController:glkVc animated:YES];
    
}
-(void)onCalibrating:(float)percent
{
    _percentLabel.text = [[NSString stringWithFormat:@"%3.1f",_percent*100]stringByAppendingString:@"%"];
    self.percent = percent;
}
-(void)onFinish
{
    openGLViewController *glkVc = [[openGLViewController alloc]init];
    glkVc.mapURL = self.mapURL;
    glkVc.whichArea = self.whichArea;
    glkVc.storeID = self.storeID;
    glkVc.currentStore = self.currentStore;
    glkVc.floorIndex = self.floorIndex;
    glkVc.path = self.path;
    [self.navigationController pushViewController:glkVc animated:YES];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [self.magCalibrator finishCalibration];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
