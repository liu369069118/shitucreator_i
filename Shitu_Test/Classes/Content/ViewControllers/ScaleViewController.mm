//
//  ScaleViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/12.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "ScaleViewController.h"
#import "ContentViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
@interface ScaleViewController ()<UIGestureRecognizerDelegate>
@property(nonatomic,strong)UILabel *distance;
@property(nonatomic,assign)CGPoint originPoint;
@property(nonatomic,assign)CGPoint leftBottomPoint;
@property(nonatomic,assign)CGPoint rightPoint;
@property(nonatomic,strong)UIImage *picImage;
@property(nonatomic,assign)CGFloat scale;

@end

@implementation ScaleViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mapView viewWillAppear];
    _mapView.delegate = self;
     _mapView.mapType = BMKMapTypeSatellite;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_mapView viewWillDisappear];
    _mapView.delegate = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //适配ios7
    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0))
    {
        self.navigationController.navigationBar.translucent = NO;
    }
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
        
        self.modalPresentationStyle=UIModalPresentationOverCurrentContext;
    }
    
    [self addCustomGestures];//添加自定义的手势
    _mapView = [[BMKMapView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:_mapView];
    [_mapView setZoomLevel:18];
    _mapView.overlookEnabled = NO;
    self.measuringScaleDelegate = self;
    self.navigationItem.backBarButtonItem.title = @"";
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(uploadImage:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    _picImage = [UIImage imageWithData:[NSData dataWithContentsOfFile:_imageFilePath]];
    UISlider *scaleSlider = [[UISlider alloc] initWithFrame:CGRectMake((Shitu_SCREENWIDTH-280)/2, Shitu_SCREENHEIGHT-150, 280, 30)];
    [scaleSlider addTarget:self action:@selector(scaleChanged:) forControlEvents:UIControlEventValueChanged];
    scaleSlider.minimumValue = 0;
    scaleSlider.maximumValue = 20;
    scaleSlider.value = 10;
    [self.view addSubview:scaleSlider];
    
    UISlider *alphaSlider = [[UISlider alloc] initWithFrame:CGRectMake(Shitu_SCREENWIDTH - 130-40, Shitu_SCREENHEIGHT/2-100, 280, 30)];
    alphaSlider.transform = CGAffineTransformMakeRotation(M_PI_2);
    [alphaSlider addTarget:self action:@selector(alphaChanged:) forControlEvents:UIControlEventValueChanged];
    alphaSlider.minimumValue = 0;
    alphaSlider.maximumValue = 20;
    alphaSlider.value = 10;
    [self.view addSubview:alphaSlider];
    
    _picImageView = [[UIImageView alloc]initWithFrame:CGRectMake(100, 100, 180, _picImage.size.height*200/_picImage.size.width)];
    _picImageView.image = [UIImage imageWithContentsOfFile:_imageFilePath];
    _picImageView.center = CGPointMake(Shitu_SCREENWIDTH/2, Shitu_SCREENHEIGHT/3);
    [self.view addSubview:_picImageView];
    // Do any additional setup after loading the view.
}
//调整大小
- (void)scaleChanged:(id)sender {
    UISlider *scaleSlider = sender;
    _picImageView.frame = CGRectMake(0, 0, (scaleSlider.value/1.67)*20 +45, ((scaleSlider.value/1.67)*20 + 45)*_picImage.size.height/_picImage.size.width);
    _picImageView.center = CGPointMake(Shitu_SCREENWIDTH/2, Shitu_SCREENHEIGHT/3);
    
}
//调整透明度
-(void)alphaChanged:(id)sender
{
    UISlider *alphaSlider = sender;
    _picImageView.alpha = (alphaSlider.value/20)*1.0 + 0.2;
}
-(void)uploadImage:(UIBarButtonItem *)sender
{
    NSString *token = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"token"];
    [self.measuringScaleDelegate changeDintanceAtMap];
    NSDictionary *parameter = @{@"t":token,@"id":_storeID,@"area":_area,@"floor_describe":_floorName,@"scale":[NSNumber numberWithFloat:_scale],@"offset_angle":@"0"};

    [self uploadImageWithParameter:parameter];
    
}

-(void)uploadImageWithParameter:(NSDictionary *)parameter
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFHTTPRequestOperation *op = [manager POST:NewFloor parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSData *imageData = [NSData dataWithContentsOfFile:_imageFilePath];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", _imageName];
        
        // 上传图片，以文件流的格式
        
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"response =%@",string);
        dispatch_async(dispatch_get_main_queue(), ^{

            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error =%@",error);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"上传失败！" message:@"请检查网络连接" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
            alertView = nil;
        });
        
    }];
    
    //上传进度
    __block MBProgressHUD *hud = [[MBProgressHUD alloc]initWithView:self.view];
    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view addSubview:hud];
            hud.labelText = @"正在上传...";
            
            //设置模式为进度框形的
            hud.mode = MBProgressHUDModeDeterminate;
            hud.progress = totalBytesWritten*1.0/totalBytesExpectedToWrite;
            [hud show:YES];
        });
    
    }];

}
#pragma mark - BMKMapViewDelegate

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(_lat, _lng);
    [_mapView setCenterCoordinate:coor animated:YES];
}

- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate {
    NSLog(@"map view: click blank");
    
}

- (void)mapview:(BMKMapView *)mapView onDoubleClick:(CLLocationCoordinate2D)coordinate {
    NSLog(@"map view: double click");
}

#pragma mark - 添加自定义的手势（若不自定义手势，不需要下面的代码）

-(void)addCustomGestures {
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTap.delegate = self;
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.cancelsTouchesInView = NO;
    doubleTap.delaysTouchesEnded = NO;
    
    [self.view addGestureRecognizer:doubleTap];
    
    /*
     *注意：
     *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
     *否则影响地图内部的手势处理
     */
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.delegate = self;
    singleTap.cancelsTouchesInView = NO;
    singleTap.delaysTouchesEnded = NO;
    [singleTap requireGestureRecognizerToFail:doubleTap];
    [self.view addGestureRecognizer:singleTap];
}
-(void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    [self.measuringScaleDelegate changeDintanceAtMap];
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)theSingleTap {
    /*
     *do something
     */
    NSLog(@"my handleSingleTap");
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)theDoubleTap {
    /*
     *do something
     */
    NSLog(@"my handleDoubleTap");
}
#pragma mark 改变地图上距离的代理方法
-(void)changeDintanceAtMap
{
    _originPoint = _picImageView.frame.origin;
    _leftBottomPoint =CGPointMake(_picImageView.frame.origin.x, _picImageView.frame.origin.y+_picImageView.frame.size.height);
    _rightPoint = CGPointMake(_picImageView.frame.origin.x + _picImageView.frame.size.width, _picImageView.frame.origin.y);
    
    CLLocationCoordinate2D originCoordinate =[_mapView convertPoint:_picImageView.frame.origin toCoordinateFromView:self.view];
    CLLocationCoordinate2D leftBottomCoordinate = [_mapView convertPoint:_leftBottomPoint toCoordinateFromView:self.view];
    CLLocationCoordinate2D rightCoordinate = [_mapView convertPoint:_rightPoint toCoordinateFromView:self.view];
    
    BMKMapPoint point1 = BMKMapPointForCoordinate(originCoordinate);
    BMKMapPoint point2 = BMKMapPointForCoordinate(leftBottomCoordinate);
    BMKMapPoint point3 = BMKMapPointForCoordinate(rightCoordinate);
    
    CLLocationDistance width = BMKMetersBetweenMapPoints(point1,point3);
    CLLocationDistance height =BMKMetersBetweenMapPoints(point1, point2);
    _scale = _picImage.size.width/width;
    
    NSLog(@"-------------%lf，%lf",width,height);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
