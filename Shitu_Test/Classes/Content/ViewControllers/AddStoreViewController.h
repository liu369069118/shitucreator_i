//
//  AddStoreViewController.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/17.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "BaseMapViewController.h"

@protocol  SaveNewStoreDelegate<NSObject>

-(void)SavaNewStoreWithCoor:(CLLocationCoordinate2D )coor;

@end

@interface AddStoreViewController : BaseMapViewController
@property(nonatomic,assign)CLLocationCoordinate2D addedStoreCoor;
@property(nonatomic,assign)id<SaveNewStoreDelegate>saveDelegate;
@end
