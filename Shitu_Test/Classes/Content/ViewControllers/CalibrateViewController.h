//
//  CorrectViewController.h
//  Shitu_Test
//
//  Created by publicshitu on 15/7/20.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreModel.h"
@interface CalibrateViewController : UIViewController

@property(nonatomic,assign)long storeID;
@property(nonatomic,assign)int whichArea;
@property(nonatomic,copy)NSString *mapURL;
@property(nonatomic,assign)int floorIndex;
@property(nonatomic,strong)StoreModel *currentStore;
@property(nonatomic,copy)NSString *path;
@end
