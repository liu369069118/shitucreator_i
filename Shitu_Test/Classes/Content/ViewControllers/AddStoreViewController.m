//
//  AddStoreViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/17.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "AddStoreViewController.h"
#import "AddStoreManager.h"
#import "MBProgressHUD.h"
@interface AddStoreViewController ()<UITextFieldDelegate>
@property(nonatomic,strong)UIView *renameView;
@property(nonatomic,strong)UITextField *name;
@property(nonatomic,strong)UILabel *coor;
@end

@implementation AddStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"识途";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(save:)];
    [self.navigationItem setRightBarButtonItem:rightItem animated:NO];
    
    [self.navigationItem.leftBarButtonItem setTitle:@"返回"];
    
    self.mapView.frame = CGRectMake(0, 0, Shitu_SCREENWIDTH, Shitu_SCREENHEIGHT*2/3);
    _renameView = [[UIView alloc]initWithFrame:CGRectMake(0, Shitu_SCREENHEIGHT*2/3, Shitu_SCREENWIDTH, Shitu_SCREENHEIGHT/3)];
    _renameView.backgroundColor = [UIColor whiteColor];
    _renameView.alpha = 0.9;
    [self.view addSubview:_renameView];
    
    _name = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, Shitu_SCREENWIDTH-20, 40)];
    _name.placeholder = @"地点名称";
    _name.borderStyle = UITextBorderStyleRoundedRect;
    _name.backgroundColor = [UIColor whiteColor];
    [self.renameView addSubview:_name];
    _name.delegate = self;
    
    _coor = [[UILabel alloc]initWithFrame:CGRectMake(0, 60, Shitu_SCREENWIDTH, 40)];
    _coor.text = [NSString stringWithFormat:@"我的经纬度：%g，%g",self.addedStoreCoor.latitude,self.addedStoreCoor.longitude];
    _coor.textColor = [UIColor blackColor];
    _coor.textAlignment = NSTextAlignmentCenter;
    [self.renameView addSubview:_coor];
    
    [self addAnnotationPoint];
    [self.mapView setCenterCoordinate:_addedStoreCoor animated:YES];
    // Do any additional setup after loading the view.
}
-(void)addAnnotationPoint
{
    BMKPointAnnotation *pointAnnotation = [[BMKPointAnnotation alloc]init];
        
    pointAnnotation.coordinate = self.addedStoreCoor;
    
    [self.mapView addAnnotation:pointAnnotation];

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGRect frame = self.view.frame;
    frame.origin.y +=150;
    frame.size.height -=150;
    self.view.frame = frame;
    [_name resignFirstResponder];

    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect frame = self.view.frame;
    frame.origin.y -=150;
    frame.size.height +=150;
    self.view.frame = frame;
    [_name becomeFirstResponder];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if ((touch.tapCount == 1)&&[_name isFirstResponder]) {
        
        CGRect frame = self.view.frame;
        frame.origin.y +=150;
        frame.size.height -=150;
        self.view.frame = frame;
        
        [_name resignFirstResponder];
        
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([_name isFirstResponder]) {
        CGRect frame = self.view.frame;
        frame.origin.y +=150;
        frame.size.height -=150;
        self.view.frame = frame;
    }
    
}
-(void)save:(id)sender
{
    if ([_name.text isEqual:@""]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:hud];
            hud.labelText = @"请输入地点名称";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });

        return;
    }
    NSString *token = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"token"];
    NSLog(@"%@",token);
    NSString *lat = [NSString stringWithFormat:@"%lf",self.addedStoreCoor.latitude];
    NSString *lng = [NSString stringWithFormat:@"%lf",self.addedStoreCoor.longitude];
    NSDictionary *parameter = @{@"t":token,@"name":_name.text,@"lat":lat,@"lng":lng};
    [[AddStoreManager shareInstance]uploadNewStoreWithNewStoreURL:NewStore tokenData:parameter];
    [self.saveDelegate SavaNewStoreWithCoor:self.addedStoreCoor];
    
    [self.navigationController popViewControllerAnimated:YES];

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
