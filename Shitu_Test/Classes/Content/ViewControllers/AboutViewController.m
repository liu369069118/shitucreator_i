//
//  AboutViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/7/8.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
-(void)viewWillAppear:(BOOL)animated
{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"关于";
    
    UIImageView *background = [[UIImageView alloc]initWithFrame:self.view.frame];
    background.image = [UIImage imageNamed:@"login_BG"];
    [self.view addSubview:background];
    
    UIImageView *title = [[UIImageView alloc]initWithFrame:CGRectMake(76/2, 164/2, 600/2, 100/2)];
    title.image = [UIImage imageNamed:@"login_welcome"];
    title.center = CGPointMake(Shitu_SCREENWIDTH/2, (164+100/2)/2);
    [self.view addSubview:title];
    
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(220/2, 360/2, 310/2, 310/2)];
    logo.center = CGPointMake(Shitu_SCREENWIDTH/2, (360+310/2)/2);
    logo.image = [UIImage imageNamed:@"login_logo"];
    [self.view addSubview:logo];
    
    UILabel *version = [[UILabel alloc]initWithFrame:CGRectMake((Shitu_SCREENWIDTH-100)/2, 335+30, 200, 30)];
    version.text = @"识途 Create 1.0";
    version.textColor = Shitu_ColorWithRgbValue(0x242424);
    version.font = [UIFont systemFontOfSize:16];
    version.textColor = [UIColor blackColor];
    [self.view addSubview:version];
    
    UILabel *copyRightEN = [[UILabel alloc]initWithFrame:CGRectMake((Shitu_SCREENWIDTH-250)/2, Shitu_SCREENHEIGHT-30, 250,20 )];
    copyRightEN.font = [UIFont systemFontOfSize:10];
    copyRightEN.textColor = Shitu_ColorWithRgbValue(0x666666);
    copyRightEN.text = @"Copyright © 2014-2015 Shitu, All Rights Reserved";
    
    UILabel *copyRightCH = [[UILabel alloc]initWithFrame:CGRectMake((Shitu_SCREENWIDTH - 143)/2, Shitu_SCREENHEIGHT-52, 143, 15)];
    copyRightCH.font = [UIFont systemFontOfSize:10];
    copyRightCH.textColor = Shitu_ColorWithRgbValue(0x666666);
    copyRightCH.text = @"北京识途科技有限公司 版权所有";
    
    [self.view addSubview:copyRightCH];
    [self.view addSubview:copyRightEN];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
