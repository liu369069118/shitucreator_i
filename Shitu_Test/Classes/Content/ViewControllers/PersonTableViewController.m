//
//  PersonTableViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/16.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "PersonTableViewController.h"

@interface PersonTableViewController ()
@property(strong,nonatomic)UIView *setView;
@property(strong,nonatomic)UIButton *setButton;
@end

@implementation PersonTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _setButton = [[UIButton alloc]initWithFrame:CGRectMake(Shitu_SCREENWIDTH-50, 20, 30, 30)];
    [_setButton setBackgroundImage:[UIImage imageNamed:@"set"] forState:UIControlStateNormal];
    [_setButton addTarget:self action:@selector(more:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_setButton];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
#pragma mark 设置
-(void)more:(UIButton *)sender
{
    
    if (!self.setView) {
        self.setView = [[UIView alloc]initWithFrame:CGRectMake(Shitu_SCREENWIDTH*2/3-5, 58, Shitu_SCREENWIDTH/3, 120)];
        self.setView.backgroundColor = [UIColor blackColor];
        self.setView.alpha = 0.6;
        
        UIButton *logout = [UIButton buttonWithType:UIButtonTypeSystem];
        logout.frame = CGRectMake(0, 0, self.setView.frame.size.width, 30);
        [logout setTitle:@"注销" forState:UIControlStateNormal];
        [logout setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [logout addTarget:self  action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *set = [UIButton buttonWithType:UIButtonTypeSystem];
        set.frame = CGRectMake(0, 30, self.setView.frame.size.width, 30);
        [set setTitle:@"设置" forState:UIControlStateNormal];
        [set setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [set addTarget:self action:@selector(set:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *account= [UIButton buttonWithType:UIButtonTypeSystem];
        account.frame = CGRectMake(0, 60, self.setView.frame.size.width, 30);
        [account setTitle:@"账号" forState:UIControlStateNormal];
        [account setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [account addTarget:self action:@selector(account:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *about = [UIButton buttonWithType:UIButtonTypeSystem];
        about.frame = CGRectMake(0, 90, self.setView.frame.size.width, 30);
        [about setTitle:@"关于" forState:UIControlStateNormal];
        [about setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [about addTarget:self action:@selector(about:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.setView addSubview:logout];
        [self.setView addSubview:set];
        [self.setView addSubview:account];
        [self.setView addSubview:about];
    }
    
    if (!self.setButton.selected) {
        [self.view addSubview:self.setView];
    }else{
        [self.setView removeFromSuperview];
    }
    self.setButton.selected = !self.setButton.selected;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)set:(UIButton *)sender
{
    
}
-(void)account:(UIButton *)sender
{
    
}
-(void)about:(UIButton *)sender
{
}
#pragma mark 注销
-(void)logout:(id)sender
{
    UIAlertView *logoutAlert = [[UIAlertView alloc]initWithTitle:@"确定注销？" message:@"点击确定注销" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [logoutAlert show];
    logoutAlert = nil;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"token"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
