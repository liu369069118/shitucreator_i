//
//  AddStoreManager.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/18.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddStoreManager : NSObject

+(AddStoreManager *)shareInstance;

-(void )uploadNewStoreWithNewStoreURL:(NSString *)newStoreURL tokenData:(NSDictionary *)data;

@end
