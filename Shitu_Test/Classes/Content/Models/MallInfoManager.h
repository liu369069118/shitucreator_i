//
//  MallInfoManager.h
//  Shitu_Test
//
//  Created by publicshitu on 15/4/30.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol storeJsonWriteDelegate <NSObject>
-(void)getStoreInfoFromServer:(NSMutableDictionary *)storeInfo;

-(void)getStoreInfoFailed:(NSError *)error;
@end
@interface MallInfoManager : NSObject

+(MallInfoManager *)shareInstance;

-(void )WriteMallInfoToSandboxWithStoreURL:(NSString *)storeURL tokenData:(NSMutableDictionary *)data;

-(NSDictionary *)getStoreJsonFromSandboxWithPathName:(NSString *)pathName;

-(NSString *)filePathWithName:(NSString *)name;

-(BOOL)getStoreInfoForStores:(NSMutableArray *)stores;

@property(nonatomic,assign)id<storeJsonWriteDelegate>storeDelegate;
@end
