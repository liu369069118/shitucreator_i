//
//  AddStoreManager.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/18.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "AddStoreManager.h"
#import "AFHTTPRequestOperationManager.h"
@implementation AddStoreManager
+(AddStoreManager *)shareInstance
{
    static AddStoreManager *addStoreManager = nil;
    
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        addStoreManager = [[self alloc] init];
    });
    return addStoreManager;
}
-(void)uploadNewStoreWithNewStoreURL:(NSString *)newStoreURL tokenData:(NSDictionary *)data
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //申明请求的数据是json类型
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    //如果报接受类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    __block NSMutableDictionary *storeInfo = nil;
    //发送请求
    [manager POST:newStoreURL parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        storeInfo = responseObject;
        NSLog(@"storeInfo:%@",storeInfo);
        [self reloadStores];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
-(void)reloadStores
{
 
}
@end
