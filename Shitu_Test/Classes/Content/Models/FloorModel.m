//
//  floorModel.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/4.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "floorModel.h"
#import "ConciseKit.h"
@implementation FloorModel
-(id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    
    if (self&&attributes) {
        
        self.floor_name = $safe([attributes objectForKey:@"floor_name"]);
        
        self.floor_id = [[attributes valueForKey:@"floor_id"]floatValue];
        
        self.floor = [[attributes valueForKey:@"floor"]floatValue];
        
        self.map_url = $safe([attributes objectForKey:@"map_url"]);
        
        self.scale = [[attributes valueForKey:@"scale"]intValue];
    }
    return  self;
}
@end
