//
//  MallInfoManager.m
//  Shitu_Test
//
//  Created by publicshitu on 15/4/30.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "MallInfoManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "StoreModel.h"
#import "FloorModel.h"
#import "ConciseKit.h"
@implementation MallInfoManager
+(MallInfoManager *)shareInstance
{
    static MallInfoManager *mallInfoManager = nil;
    
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        mallInfoManager = [[self alloc] init];
    });
    return mallInfoManager;
}
//数据解析
-(void )WriteMallInfoToSandboxWithStoreURL:(NSString *)storeURL tokenData:(NSMutableDictionary *)data;
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //申明请求的数据是json类型
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    //如果报接受类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    __block NSMutableDictionary *storeInfo = nil;
    //发送请求
    [manager POST:storeURL parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"storeInfo: %@", responseObject);
        storeInfo = responseObject;
        
        [self.storeDelegate getStoreInfoFromServer:storeInfo];
        [self performSelectorInBackground:@selector(writeJsonToSandbox:) withObject:storeInfo];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
        NSString *pathName = [NSString stringWithFormat:@"%@storeInfo",userName];
        NSString *path = [self filePathWithName:pathName];
        //移除旧文件
        [[NSFileManager defaultManager]removeItemAtPath:path error:nil];
        
        [self.storeDelegate getStoreInfoFailed:error];
    }];
}

-(void)writeJsonToSandbox:(NSMutableDictionary *)JsonString
{
    NSError *error;
    NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
    NSString *pathName = [NSString stringWithFormat:@"%@storeInfo",userName];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:JsonString options:NSJSONWritingPrettyPrinted error:&error];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *Json_path=[self filePathWithName:pathName];
    
    //==写入文件
    [str writeToFile:Json_path atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
}
//json命名
-(NSString *)filePathWithName:(NSString *)name
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path=[paths objectAtIndex:0];
    NSString *Json_path=[path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.json",name]];
    return Json_path;
}
-(NSDictionary *)getStoreJsonFromSandboxWithPathName:(NSString *)pathName;
{
    NSString *path =[ self filePathWithName:pathName];
    //==Json数据
    NSData *data=[NSData dataWithContentsOfFile:path];
    //==JsonObject
    NSError *error = nil;
    id JsonObject=[NSJSONSerialization JSONObjectWithData:data
                                                  options:NSJSONReadingAllowFragments
                                                    error:&error];
    return JsonObject;
}

-(BOOL)getStoreInfoForStores:(NSMutableArray *)stores
{
    NSString *userName = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"name"];
    NSDictionary *storeInfo = [[MallInfoManager shareInstance]getStoreJsonFromSandboxWithPathName:[NSString stringWithFormat:@"%@storeInfo",userName]];
    
    if ([[storeInfo objectForKey:@"auth"]isEqualToString:@"ok"]) {
        
        NSArray *json = [NSArray arrayWithArray:[storeInfo objectForKey:@"json"]];
        for (NSDictionary *storeDic in json) {
            //            NSLog(@"json = %@",json);
            StoreModel  *storeModel = [[StoreModel alloc]initWithAttributes:storeDic];
            NSArray *floors = $safe([storeDic objectForKey:@"floors"]);
            storeModel.floors = [NSMutableArray array];
            for (NSDictionary *floor in floors) {
                
                FloorModel *floorModel = [[FloorModel alloc]initWithAttributes:floor];
                [storeModel.floors addObject:floorModel];
                
            }
            [stores addObject:storeModel];
        }
    }
    
    return YES;
    
}
@end
