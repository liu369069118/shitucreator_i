//
//  storeModel.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/4.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreModel : NSObject

@property(nonatomic,strong)NSMutableArray *floors;

@property(nonatomic,copy)NSString *key;

@property(nonatomic,assign)float lat;

@property(nonatomic,assign)float lng;

@property(nonatomic,copy)NSString *name;

@property(nonatomic,assign)long storeID;

-(id)initWithAttributes:(NSDictionary *)attributes;

@end
