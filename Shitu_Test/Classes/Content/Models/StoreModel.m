//
//  storeModel.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/4.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "storeModel.h"
#import "ConciseKit.h"
@implementation StoreModel
-(id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    
    if (attributes&&self) {
        
        self.key = $safe([attributes objectForKey:@"key"]);
        
        self.lat = [[attributes valueForKey:@"lat"]floatValue];
        
        self.lng = [[attributes valueForKey:@"lng"]floatValue];
        
        self.name = $safe([attributes objectForKey:@"name"]);
        
        self.storeID = [[attributes valueForKey:@"mall_id"]longValue];
    }
    
    return self;
}
@end
