//
//  floorModel.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/4.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FloorModel : NSObject

@property(nonatomic,copy)NSString *map_url;

@property(nonatomic,assign)float floor;

@property(nonatomic,assign)float floor_id;

@property(nonatomic,copy)NSString *floor_name;

@property(nonatomic,assign)int scale;
//@property(nonatomic,copy)NSString *map_url;

-(id)initWithAttributes:(NSDictionary *)attributes;

@end
