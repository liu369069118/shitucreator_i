//
//  openGLViewController.h
//  GL
//
//  Created by publicshitu on 15/5/20.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "SHTSensors.h"
#import "StoreModel.h"
@interface openGLViewController : GLKViewController<SHTSensorDelegate>
@property(nonatomic,strong)NSMutableArray *lines;
@property(nonatomic,assign)long storeID;
@property(nonatomic,strong)UIImage *image;
@property(nonatomic,assign)int whichArea;
@property(nonatomic,copy)NSString *mapURL;
@property(nonatomic,assign)int floorIndex;
@property(nonatomic,strong)StoreModel *currentStore;
@property(nonatomic,copy)NSString *path;
@property(nonatomic)GLKMatrixStackRef textureMatrixStack;
@end
