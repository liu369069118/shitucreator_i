//
//  openGLViewController.m
//  GL
//
//  Created by publicshitu on 15/5/20.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "openGLViewController.h"
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <GLKit/GLKTextureLoader.h>
#import "MBProgressHUD.h"
#import "SHTNatureCollectData.h"
#import "SHTOrientation.h"
#import "SHTSmallFloat.h"
#import "GLSHTDB.h"
#import "NSString+URL.h"
#import "PathBuilderView.h"
#import "DrawPath.h"
#import "SaveAndClearView.h"
#import "LocationViewController.h"
@interface openGLViewController ()<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    // 用作模拟headingsensor
    BOOL _useHeadingOverMag;
    volatile float _headingX;
    volatile float _headingY;
    volatile float _headingZ;
    volatile float _azimuth;
    volatile float _pitch;
    volatile float _roll;
    volatile BOOL _run;
    
    volatile float _screenAspect;
    
    BOOL _isInMoveMode;
    
    BOOL _isFirst2Fingers;
    
    BOOL _isBarItemSelected;
    
    BOOL _islineManager;
    
    volatile float _rotateAngle;
    
    float _initDistance;
    
    float _initAngel;
    
    float _scaleSave;
    
    float _angelSave;
    
    volatile float _sumX;
    
    volatile float _sumY;
    
    volatile float _zoomScale;
    int lineIndex;
    
    BOOL iscellSelected;
    
    BOOL isDeleteUpdate;
    
    BOOL isSuperUser;
    
    GLuint _vertexBuffer;
    GLuint _indexBuffer;
    
    GLuint _lineBuffer;
    
    NSMutableArray * _recordArray;
    
    volatile GLSHTDB * _db;
    volatile BOOL _isRecording;
    volatile int _ruleCount;
    
    NSOperationQueue * _runQueue;
    
    GLKView *glkView;
}
@property (nonatomic) SHTSensors * sensor;

@property(nonatomic,strong)EAGLContext *context;
@property(strong,nonatomic)GLKBaseEffect *effect;
@property(nonatomic,assign)CGPoint beginPoint;
@property(nonatomic,assign)CGPoint transformedPoint;

@property(nonatomic,assign)CGPoint centerPoint;

@property(nonatomic,assign)CGFloat distanceX;
@property(nonatomic,assign)CGFloat distanceY;
//缩放
@property(nonatomic,strong)NSMutableArray *touchs;

@property(nonatomic,strong)GLKTextureInfo *textureInfo;
@property(nonatomic,strong)GLKTextureInfo *flagTexture;
@property(nonatomic,strong)GLKTextureInfo *lineDelTexture;

@property(nonatomic,strong)DrawPath *drawingPath;

@property(nonatomic,assign)CGFloat distanceOfTwoPoint;
@property(nonatomic,assign)int POIAllCount;

@property(nonatomic,strong)UILabel *CoordinateLabel;
@property(nonatomic,strong)UILabel *POILabel;

@property(nonatomic,strong)SaveAndClearView *saveView;
@property(nonatomic,assign)int allRecordCount;

@property(nonatomic,strong)PathBuilderView *pathView;
@property(nonatomic,assign)CGSize imageSize;

@property(nonatomic,assign)int whichLine;

@property(nonatomic,strong)UITableView *lineManagerTableView;
@property(nonatomic,assign)int nodeCountOfLine;

@property(nonatomic,strong)NSMutableArray *linesArray;

@property(nonatomic,strong)UILabel *sensorLabel;

@property(nonatomic,strong)UIImage *flag;

@property(nonatomic,strong)UIImage *lineDel;
@end

@implementation openGLViewController
static GLfloat texCoords[]={
    0,0,
    1,0,
    0,1,
    1,1,
    
};
static GLfloat vertices[] = {
    -1, -1,//左下
    1, -1,//右下
    1, 1,//右上
    -1, 1,//左上
};
static GLfloat fragVertices[] = {
    -0.02, -0.02,//左下
    0.02, -0.02,//右下
    -0.02, 0.02,//右上
    0.02, 0.02,//左上
};
static GLfloat lineDelVertices[] = {
    -0.02, -0.02,//左下
    0.02, -0.02,//右下
    -0.02, 0.02,//右上
    0.02, 0.02,//左上
};
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    isDeleteUpdate = NO;
}
-(void)viewDidAppear:(BOOL)animated
{
    //传感器
    [super viewDidAppear:animated];
    [self prepareSensor];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _useHeadingOverMag = YES;
    
    _run = YES;
    
    _lines = [NSMutableArray array];
    _drawingPath = [[DrawPath alloc]init];
    
    _drawingPath.nodes = [NSMutableArray array];
    [_lines addObject:_drawingPath];
    
    _db = [[GLSHTDB alloc]init];
    [_db openWithStoreId:(int)_storeID];
    [_db createNesscery];
    
    _whichLine = [_db maxLine:_whichArea];
    
    _allRecordCount = [_db countOfFloor:_whichArea];
    [self selectNodes];
    
    _isBarItemSelected = NO;
    _islineManager = NO;
    iscellSelected = NO;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemReply target:self action:@selector(popToRootViewController)];
    
    UIBarButtonItem *appendOroveride = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"btn_upload_to_server"] style:UIBarButtonItemStylePlain target:self action:@selector(appendOrOverwrite:)];
    
    UIBarButtonItem *saveOrDel = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"has_collect_routing_delete_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(saveOrDel:)];
    
    UIBarButtonItem *lineManager = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"has_collect_routing_luxian_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(lineManager:)];
    
    //高级用户
    isSuperUser = (BOOL)[[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]valueForKey:@"su"];
    if (isSuperUser) {
        self.navigationItem.rightBarButtonItems =  [NSArray arrayWithObjects:lineManager,saveOrDel,appendOroveride, nil];
    }
    else{
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:lineManager,saveOrDel, nil];
    }
    //glkView
    [self prepareGLKView];
    
    if(_useHeadingOverMag){
        NSThread * simulateHeadingThread = [[NSThread alloc]initWithTarget:self selector:@selector(simulateHeadingSensor) object:nil];
        [simulateHeadingThread start];
    }
    
}

-(void)selectNodes
{
    _linesArray = [NSMutableArray arrayWithArray:[_db selectLinesWithArea:_whichArea]];
    
    for (int i=0; i< _linesArray.count; i++) {
        
        DrawPath *drawPath = [_db selectNodeFromDBWithArea:_whichArea line:[_linesArray[i]intValue]];
        
        [_lines addObject:drawPath];
        drawPath = nil;
    }
    
    
}

-(void)lineManager:(id )sender
{
    if (_isRecording) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
            [self.view addSubview:hud];
            hud.labelText = @"正在记录中..";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
        return;
    }
    if (_isBarItemSelected) {
        _isBarItemSelected = !_isBarItemSelected;
        [_saveView removeFromSuperview];
    }
    _islineManager = !_islineManager;
    if (!_lineManagerTableView) {
        _lineManagerTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Shitu_SCREENWIDTH, Shitu_SCREENHEIGHT/4) style:UITableViewStylePlain];
        _lineManagerTableView.delegate = self;
        _lineManagerTableView.dataSource = self;
    }
    if (_islineManager) {
        [self.view addSubview:_lineManagerTableView];
    }
    else
    {
        iscellSelected = NO;
        [_lineManagerTableView removeFromSuperview];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _linesArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdetify = @"linesCell";
    UITableViewCell *cell = [_lineManagerTableView dequeueReusableCellWithIdentifier:reuseIdetify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdetify];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        cell.showsReorderControl = YES;
        
    }
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text =[NSString stringWithFormat:@"路线%@", _linesArray[indexPath.row]];
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    lineIndex = (int)indexPath.row;
    iscellSelected = YES;
     [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 删除操作
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // 1.删除数据
        if (_linesArray.count < (int )indexPath.row) {
            NSLog(@"linesArrayCount = %d",(int)_linesArray.count);
        }
        isDeleteUpdate  = YES;
        
        int count = [_db countOfDeleteRule:[_linesArray[indexPath.row]intValue] area:_whichArea];
        
        int deleteMaxRule =[_db selectMaxRuleWithDeleteLine:[_linesArray[indexPath.row]intValue] area:_whichArea];
        
        int maxRule = [_db maxRule];
        
        [_db deleteLine:[_linesArray[indexPath.row]intValue] fromArea:_whichArea];
        
        NSLog(@"deleteMaxRule(%d,%d)",deleteMaxRule,maxRule);
        if (deleteMaxRule < maxRule) {
        
            [_db updateDataBaseInOrderMaxRuleWithDeleteLine:deleteMaxRule count:count];
            
        }
        _ruleCount = [_db maxRule]+1;
        _whichLine = [_db maxLine:_whichArea];
        
        [self.lines removeObjectAtIndex:indexPath.row+1];
        [_linesArray removeObjectAtIndex:indexPath.row];
        
        isDeleteUpdate = NO;
        // 2.更新UITableView UI界面
        [tableView reloadData];
        
    }
    
}

-(void)appendOrOverwrite:(id)sender
{
    if (_isRecording) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
            [self.view addSubview:hud];
            hud.labelText = @"正在记录中..";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
        return;
    }
    if (_islineManager) {
        _islineManager = !_islineManager;
        [_lineManagerTableView removeFromSuperview];
    }
    if (_isBarItemSelected) {
        _isBarItemSelected = !_isBarItemSelected;
        [_saveView removeFromSuperview];
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请问" message:@"你是想重写还是追加呢？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"重写",@"追加", nil];
    [alert show];
    alert = nil;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
            //覆盖云端
            [self uploadDB];
            break;
        case 2:
            //追加到云端
            [self appendDB];
            break;
        default:
            break;
    }
}
-(void)saveOrDel:(id)sender
{
    if (_isRecording) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
            [self.view addSubview:hud];
            hud.labelText = @"正在记录中..";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
        return;
    }
    if (_islineManager) {
        _islineManager = !_islineManager;
        [_lineManagerTableView removeFromSuperview];
    }
    _isBarItemSelected = !_isBarItemSelected;
    
    if (!_saveView) {
        if (isSuperUser) {
            self.saveView = [[SaveAndClearView alloc]initWithFrame:CGRectMake(Shitu_SCREENWIDTH*2/3-5, 64, Shitu_SCREENWIDTH/3, 120)];
        }
        else{
        self.saveView = [[SaveAndClearView alloc]initWithFrame:CGRectMake(Shitu_SCREENWIDTH*2/3-5, 64, Shitu_SCREENWIDTH/3, 80)];
            [_saveView.clearCloud removeFromSuperview];
        }
        self.saveView.backgroundColor = [UIColor blackColor];
        self.saveView.alpha = 0.6;
        
        [self.saveView.clearLocal addTarget:self  action:@selector(clearLocal:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.saveView.clearArea addTarget:self action:@selector(clearArea:) forControlEvents:UIControlEventTouchUpInside];
    
        [self.saveView.clearCloud addTarget:self action:@selector(clearCloud:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    if (_isBarItemSelected) {
        
        [self.view addSubview:self.saveView];
        
    }else{
        [self.saveView removeFromSuperview];
    }
    
    
}
-(void)clearLocal:(UIButton *)sender
{
        _ruleCount = 0;
        [_db deleteAllLocal];
        [_drawingPath.nodes removeAllObjects];
        [_lines removeAllObjects];
        _allRecordCount = 0;
        
        _drawingPath = [[DrawPath alloc]init];
        _drawingPath.nodes = [NSMutableArray array];
        [_lines addObject:_drawingPath];
        //清除tableView的路线
        [_linesArray removeAllObjects];
        _whichLine = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
            [self.view addSubview:hud];
            hud.labelText = @"已清除本地数据";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
  
}
-(void)clearArea:(UIButton *)sender
{
        
        [_db deleteArea:_whichArea];
        [_drawingPath.nodes removeAllObjects];
        [_lines removeAllObjects];
        _allRecordCount = 0;
        
        _drawingPath = [[DrawPath alloc]init];
        _drawingPath.nodes = [NSMutableArray array];
        [_lines addObject:_drawingPath];
        
        //清除tableView的路线
        [_linesArray removeAllObjects];
        _whichLine = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
            [self.view addSubview:hud];
            hud.labelText = @"已清除本层数据";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
}
-(void)clearCloud:(UIButton *)sender
{
        
    [self deleteTheCloud];
    
}
#pragma mark sensor

-(void)prepareSensor
{
    self.sensor = [[SHTSensors alloc]initWithInterval:0.1f];
    self.sensor.delegate = self;
    
    _runQueue = [[NSOperationQueue alloc]init];
    
    [self.sensor registerDeviceMotionSensor];
    
    if(_useHeadingOverMag){
        [self.sensor registerCompass];
    }
    _recordArray = [[NSMutableArray alloc]init];
    
    _isRecording = NO;
    if([_db countOfTable:@"mag"] == 0){
        _ruleCount = 0;
    }else{
        _ruleCount = [_db maxRule] + 1;
    }
    [self.sensor start];
}
#pragma mark glkView
-(void)prepareGLKView
{
    
    
    self.view.userInteractionEnabled = YES;
    _zoomScale = 1;
    _scaleSave = 1;
    _angelSave = 0;
    self.touchs = [NSMutableArray array];
    //context
    
    self.context = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (!self.context) {
        NSLog(@"failed to creat ES context");
    }
    else if (![EAGLContext setCurrentContext:self.context])
    {
        NSLog(@"failed to set ES context current");
    }
    //GLKView
    glkView = [[GLKView alloc]initWithFrame:[UIScreen mainScreen].bounds context:self.context];
    self.view = glkView;
    //drawLineButton
    _pathView = [[PathBuilderView alloc]initWithFrame:CGRectMake(0, Shitu_SCREENHEIGHT-100, Shitu_SCREENWIDTH, 100)];
    _pathView.backgroundColor = Shitu_ColorWithRgbValue(0x262e34);
    _pathView.alpha = 0.7;
    [_pathView.drawLine addTarget:self action:@selector(drawLine:) forControlEvents:UIControlEventTouchUpInside];
    [_pathView.record addTarget:self action:@selector(record:) forControlEvents:UIControlEventTouchUpInside];
    [_pathView.clean addTarget:self action:@selector(clean:) forControlEvents:UIControlEventTouchUpInside];
    
    _pathView.record.enabled = NO;
    _pathView.clean.enabled = NO;
    [glkView addSubview:_pathView];
    //定位button
    UIButton *locationBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    locationBtn.frame = CGRectMake(15, Shitu_SCREENHEIGHT-150, 48, 48);
    locationBtn.backgroundColor = [UIColor clearColor];
    [locationBtn setBackgroundImage:[UIImage imageNamed:@"home_positioning"] forState:UIControlStateNormal];
    [locationBtn addTarget:self  action:@selector(location:) forControlEvents:UIControlEventTouchUpInside];
    [glkView addSubview:locationBtn];
    
    //显示坐标及信息点个数
    _CoordinateLabel = [[UILabel alloc]initWithFrame:CGRectMake(Shitu_SCREENWIDTH-83, 64+12,83, 19)];
    _CoordinateLabel.backgroundColor = Shitu_ColorWithRgbValue(0x00baf0);
    _CoordinateLabel.font = [UIFont systemFontOfSize:10];
    _CoordinateLabel.textAlignment = NSTextAlignmentCenter;
    _CoordinateLabel.textColor = [UIColor whiteColor];
    [glkView addSubview:_CoordinateLabel];
    
    //    _POILabel = [[UILabel alloc]initWithFrame:CGRectMake(Shitu_SCREENWIDTH-100, 64, 83, 19)];
    //    _POILabel.backgroundColor = Shitu_ColorWithRgbValue(0x00baf0);
    //    _POILabel.font = [UIFont systemFontOfSize:12];
    //    _POILabel.textColor = [UIColor whiteColor];
    //    [glkView addSubview:_POILabel];
    
    //    _sensorLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 94, Shitu_SCREENWIDTH, 30)];
    //    _sensorLabel.backgroundColor = [UIColor redColor];
    //    _sensorLabel.alpha = 0.6;
    //    _sensorLabel.font = [UIFont systemFontOfSize:12];
    //    _sensorLabel.textColor = [UIColor whiteColor];
    //    [glkView addSubview:_sensorLabel];
    
    [EAGLContext setCurrentContext:self.context];
    //设置帧数
    self.preferredFramesPerSecond = 60;
    
    glEnable(GL_DEPTH_TEST);
    
    self.effect = [[GLKBaseEffect alloc]init];
    //创建矩阵栈
    self.textureMatrixStack = GLKMatrixStackCreate(kCFAllocatorDefault);
    GLKMatrixStackLoadMatrix4(self.textureMatrixStack, self.effect.transform.modelviewMatrix);
    
    CGSize size = self.view.bounds.size;
    _screenAspect = fabs(size.width/size.height);
    //底图
    NSData *data = [NSData dataWithContentsOfFile:_path];
    _image = [UIImage imageWithData:data];
    self.textureInfo = [self loadImage:_image];
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:@"flag" ofType:@"png"];
    _flag = [UIImage imageWithContentsOfFile:path];
    self.flagTexture = [self loadImage:_flag];

    _lineDel = [UIImage imageNamed:@"line_delete"];
    self.lineDelTexture = [self loadImage:_lineDel];
    
    //定位点图
    //初始化栈
    
    GLKMatrix4 pro = GLKMatrix4Identity;
    
    pro = GLKMatrix4Scale(pro, 1.0f/_screenAspect, 1.0f, 1.0f);
    self.effect.transform.projectionMatrix = pro;
    [self drawCenterPoint];
    
    _isInMoveMode = YES;
    
    vertices[0] = -_image.size.width/_image.size.height;
    vertices[2] = _image.size.width/_image.size.height;
    vertices[4] = -_image.size.width/_image.size.height;
    vertices[6] = _image.size.width/_image.size.height;
    _imageSize = CGSizeMake(_image.size.width, _image.size.height);
    
    
}
#pragma mark 定位触发事件
-(void)location:(UIButton *)sender
{
    if (_isRecording) {
        return;
    }
    LocationViewController *locationVc = [[LocationViewController alloc]init];
    locationVc.whichArea = _whichArea;
    locationVc.currentStore = _currentStore;
    locationVc.floorIndex = _floorIndex;
    locationVc.path = self.path;
    [self.navigationController pushViewController:locationVc animated:YES];
    
}
//贴位图
-(GLKTextureInfo *)loadImage:(UIImage *)image{
    
    GLKTextureInfo *info;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],GLKTextureLoaderOriginBottomLeft, nil];
    return info = [GLKTextureLoader textureWithCGImage:image.CGImage options:options error:nil];
    
}
#pragma mark glkView监听
-(void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    
    glClearColor(0.5, 0.5, 0.5, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    GLKMatrixStackPush(self.textureMatrixStack);
    
    GLKMatrixStackTranslate(self.textureMatrixStack, _sumX, _sumY, 0);
    GLKMatrixStackScale(self.textureMatrixStack, _zoomScale, _zoomScale, 0.0f);
    GLKMatrixStackRotate(self.textureMatrixStack, -_rotateAngle, 0.0f, 0.0f, 1.0f);
    
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    [self drawCGImage];
    
    if ([self isBeyondBounds]) {
        _CoordinateLabel.text = @"超出范围";
    }
    else
    {
        CGPoint screenPoint = [self convertToScreenPointWithGLPoint:_transformedPoint];
        _CoordinateLabel.text = [NSString stringWithFormat:@"(%4.1f,%4.1f)",(screenPoint.x -(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT, screenPoint.y*_imageSize.height/Shitu_SCREENHEIGHT];
    }
    _POILabel.text = [NSString stringWithFormat:@"已采集%d",_allRecordCount];
    
    
    [self drawLineWithPoint];
    
    if (_drawingPath.nodes.count>0) {
        [self drawflagWithPoint:[self convertGLPointWithPixPoint:[_drawingPath.nodes[0]CGPointValue]]];
    }
    if (iscellSelected) {
        [self drawDelLine:lineIndex];
    }
    GLKMatrixStackPop(self.textureMatrixStack);
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
}
-(void)drawDelLine:(int)line
{
    DrawPath *drawPath =  _lines[lineIndex+1];
    CGPoint point = [self convertGLPointWithPixPoint:[drawPath.nodes[0]CGPointValue]];
    
    self.effect.texture2d0.enabled = YES;
    //设置纹理
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    self.effect.texture2d0.name = _lineDelTexture.name;
    self.effect.texture2d0.target = _lineDelTexture.target;
    
    lineDelVertices[0] = point.x-0.02;
    lineDelVertices[1] = point.y-0.02;
    lineDelVertices[2] = point.x+0.02;
    lineDelVertices[3] = point.y-0.02;
    lineDelVertices[4] = point.x-0.02;
    lineDelVertices[5] = point.y+0.02;
    lineDelVertices[6] = point.x+0.02;
    lineDelVertices[7] = point.y+0.02;
    
    
    //开始绘制
    [self.effect prepareToDraw];
    glEnable(GL_BLEND);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _indexBuffer);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, lineDelVertices);
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    
    
    
}
-(void)drawflagWithPoint:(CGPoint)point
{
    self.effect.texture2d0.enabled = YES;
    //设置纹理
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    self.effect.texture2d0.name = _flagTexture.name;
    self.effect.texture2d0.target = _flagTexture.target;
    
    fragVertices[0] = point.x-0.02;
    fragVertices[1] = point.y-0.02;
    fragVertices[2] = point.x+0.02;
    fragVertices[3] = point.y-0.02;
    fragVertices[4] = point.x-0.02;
    fragVertices[5] = point.y+0.02;
    fragVertices[6] = point.x+0.02;
    fragVertices[7] = point.y+0.02;
    
    
    //开始绘制
    [self.effect prepareToDraw];
    glEnable(GL_BLEND);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _indexBuffer);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, fragVertices);
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    
}
#pragma mark button事件
-(void)drawLine:(UIButton *)sender
{
    if (![self isBeyondBounds]&&!_isRecording) {
        
        [_drawingPath.nodes addObject:[NSValue valueWithCGPoint:[self convertPixPointWithGLPoint:_transformedPoint]]];
        if (_drawingPath.nodes.count>1) {
            _pathView.clean.enabled = YES;
            _pathView.record.enabled = YES;
        }
    }
    
}
-(void)clean:(UIButton *)sender
{
    if (_isRecording) {
        return;
    }
    [_drawingPath.nodes removeAllObjects];
    _pathView.drawLine.enabled = YES;
    _pathView.record.enabled = NO;
    _pathView.clean.enabled = NO;
}

-(void)record:(UIButton *)sender
{
    if (_drawingPath.nodes.count <2) {
        return;
    }
    sender.selected = !sender.selected;
    _isRecording = !_isRecording;
    _POIAllCount = 0;
    if(!_isRecording){
        _pathView.drawLine.enabled = YES;
        
        // do saving
        for (int i = 0; i<_drawingPath.nodes.count-1; i++) {
            CGPoint firstPoint = [self convertToScreenPointWithGLPoint:[self convertGLPointWithPixPoint:[_drawingPath.nodes[i]CGPointValue]]];
            CGPoint lastPoint = [self convertToScreenPointWithGLPoint:[self convertGLPointWithPixPoint:[_drawingPath.nodes[i+1]CGPointValue]]];
            
            CGFloat distance = [self distanceBetweenPointA:firstPoint andPointB:lastPoint];
            _distanceOfTwoPoint += distance;
            
        }
        
        
        long sizeSum = 0;
        _whichLine ++;
        for (int i=0; i<_drawingPath.nodes.count - 1; i++) {
            
            CGPoint firstPoint = [self convertToScreenPointWithGLPoint:[self convertGLPointWithPixPoint:[_drawingPath.nodes[i] CGPointValue]]];
            CGPoint lastPoint =[self convertToScreenPointWithGLPoint:[self convertGLPointWithPixPoint: [_drawingPath.nodes[i+1] CGPointValue]]];
            CGFloat distance = [self distanceBetweenPointA:firstPoint andPointB:lastPoint];
            
            CGFloat unitX = (lastPoint.x - firstPoint.x)*_distanceOfTwoPoint/(_recordArray.count*distance);
            CGFloat unitY = (lastPoint.y - firstPoint.y)*_distanceOfTwoPoint/(_recordArray.count*distance);
            //这条线段上的信息点个数
            
            int POIcount = (int )((_recordArray.count*distance + _distanceOfTwoPoint - 1)/_distanceOfTwoPoint);
            
            sizeSum += POIcount;
            
            for(int j = 0; j < POIcount; j++){
                
                if (j+_POIAllCount<_recordArray.count) {
                    
                    SHTNatureCollectData * data = [_recordArray objectAtIndex:j+_POIAllCount];
                    
                    data.line = _whichLine;
                    
                    
                    if (j == POIcount-1||j+_POIAllCount==_recordArray.count -1) {
                        
                        data.px = (lastPoint.x -(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT;
                        data.py = lastPoint.y*_imageSize.height/Shitu_SCREENHEIGHT;
                        data.node = 1;
                    }
                    else if (j== 0)
                    {
                        if (i==0) {
                            data.node = 1;
                            data.px = (firstPoint.x -(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT;
                            data.py = firstPoint.y*_imageSize.height/Shitu_SCREENHEIGHT;
                            
                        }else
                        {
                            data.px = ((firstPoint.x + unitX*(j+1)) -(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT;
                            data.py = (firstPoint.y + unitY*(j+1))*_imageSize.height/Shitu_SCREENHEIGHT;
                        }
                    }
                    else
                    {
                        if (i==0) {
                            data.px = ((firstPoint.x + unitX*j)-(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT;
                            data.py = (firstPoint.y + unitY*j)*_imageSize.height/Shitu_SCREENHEIGHT;
                        }
                        else
                        {
                            data.px = ((firstPoint.x + unitX*(j+1)) -(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT;
                            data.py = (firstPoint.y + unitY*(j+1))*_imageSize.height/Shitu_SCREENHEIGHT;
                            
                        }
                        
                    }
                    NSLog(@"data(x,y,node) = (%f,%f,%d)",data.px,data.py,data.node);
                    if( j + _POIAllCount == [_recordArray count] - 1){
                        data.isend = 1;
                    }
                    data = nil;
                }
            }
            _POIAllCount += POIcount;
            NSLog(@"----------------");
        }
        
        NSLog(@"all count=%lu, calc count=%lu", [_recordArray count], sizeSum);
        
        
        if ([_db isRuleNotInOrder:_whichArea]) {
            
            int maxRule  = [_db maxRuleWithArea:_whichArea];
            int otherCount = [_db countOfOtherRule:_whichArea];
            
            [_db updateDataBaseInOrderWithMaxRule:maxRule recordsCount:(int)_recordArray.count];
            [_db insertMany:_recordArray];
            [_db updateDataBaseInOrderWithOtherRule:_whichArea count:maxRule otherCount:otherCount];
            
            _ruleCount = [_db maxRule]+1;
        }
        else
        {
            [_db insertMany:_recordArray];
        }
        [_linesArray addObject:[NSNumber numberWithInt: _whichLine]];
        
        DrawPath *drawPath = [[DrawPath alloc]init];
        drawPath.nodes = [_drawingPath.nodes copy];
        [_lines addObject:drawPath];
        [_drawingPath.nodes removeAllObjects];
        
        
        NSLog(@"completing saving");
        _pathView.record.enabled = NO;
        _pathView.clean.enabled = NO;
    }
    else{
        _pathView.clean.enabled = NO;
        _pathView.drawLine.enabled = NO;
        [_recordArray removeAllObjects];
    }
    _distanceOfTwoPoint = 0;
    
}
#pragma mark 传感器回调
- (void)didGotDeviceMotionData:(CMDeviceMotion *)motion OrError:(NSError *)error
{
    if(_isRecording){
        if(!_useHeadingOverMag){
            SHTNatureCollectData * data = [[SHTNatureCollectData alloc]init];
            
            data.x = [SHTSmallFloat toShortWithSmallFloat:motion.magneticField.field.x];
            data.y = [SHTSmallFloat toShortWithSmallFloat:motion.magneticField.field.y];
            data.z = [SHTSmallFloat toShortWithSmallFloat:motion.magneticField.field.z];
            data.azimuth = _azimuth;
            data.pitch = -motion.attitude.pitch * 180 / M_PI;
            data.roll = motion.attitude.roll * 180 / M_PI;
            data.isend = 0;
            data.rule = _ruleCount++;
            data.line = 0;
            data.node = 0;
            data.px = 0.0f;
            data.py = 0.0f;
            data.area = self.whichArea;
            data.pressure = -1;
            
            [_recordArray addObject:data];
            //已采集信息点个数（从传感器获得）
            _allRecordCount ++;
            
            NSLog(@"mag data mx= %f, my=%f,mz=%f",motion.magneticField.field.x, motion.magneticField.field.y, motion.magneticField.field.z);
        }else{
            
        }
    }
    
    _pitch = - motion.attitude.pitch * 180 / M_PI;
    _roll = motion.attitude.roll * 180 / M_PI;
}

- (void)didGotHeadingData:(CLHeading *)heading
{
    _headingX = heading.x;
    _headingY = heading.y;
    _headingZ = heading.z;
    
    _azimuth = heading.magneticHeading + 180;
}

- (void)simulateHeadingSensor
{
    while(_run){
        [self didGotHeadingSimulateX:_headingX AndY:_headingY AndZ:_headingZ];
        [NSThread sleepForTimeInterval:0.1f];
    }
}

- (void)didGotHeadingSimulateX:(float)mx AndY:(float)my AndZ:(float)mz
{
    if(_isRecording){
        
        SHTNatureCollectData * data = [[SHTNatureCollectData alloc]init];
        
        data.x = [SHTSmallFloat toShortWithSmallFloat:mx];
        data.y = [SHTSmallFloat toShortWithSmallFloat:my];
        data.z = [SHTSmallFloat toShortWithSmallFloat:mz];
        data.azimuth = _azimuth;
        data.pitch = _pitch;
        data.roll = _roll;
        data.isend = 0;
        data.rule = _ruleCount++;
        NSLog(@"data rule = %d", data.rule);
        data.line = 0;
        data.node = 0;
        data.px = 0.0f;
        data.py = 0.0f;
        data.area = self.whichArea;
        data.pressure = -1;
        
        [_recordArray addObject:data];
        //已采集信息点个数（从传感器获得）
        _allRecordCount ++;
        
        NSLog(@"data got from headingX= %f, headingY=%f, headingZ=%f, rule=%d",mx, my, mz, _ruleCount);
        data = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        _sensorLabel.text = [NSString stringWithFormat:@"(x=%4.2f, y=%4.2f, z=%4.2f, a=%4.2f, p=%4.2f, r=%4.2f)",mx,my,mz,_azimuth, _pitch, _roll];
    });
    
}



- (void)uploadDB
{
    NSThread * runThread = [[NSThread alloc]initWithTarget:self selector:@selector(doUpload) object:nil];
    [runThread start];
}
- (void)doUpload
{
    if ([_db overrideCloud]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
            [self.view addSubview:hud];
            hud.labelText = @"上传成功";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(2);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
        
    }else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"上传失败！" message:@"请检查网络是否连接" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
            alertView = nil;
        });
    }
}
-(void)appendDB
{
    NSThread * runThread = [[NSThread alloc]initWithTarget:self selector:@selector(doappendCloud) object:nil];
    [runThread start];
    
}
-(void)doappendCloud
{
    if ([_db appendToCloud]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
            [self.view addSubview:hud];
            hud.labelText = @"追加成功";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(2);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"追加失败！" message:@"请检查网络是否连接" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
            alertView = nil;
        });
    }
}
-(void)deleteTheCloud
{
    NSThread * runThread = [[NSThread alloc]initWithTarget:self selector:@selector(doDeleteCloud) object:nil];
    [runThread start];
    
}
-(void)doDeleteCloud
{
    NSLog(@"我是错误%p",_db);
    if([_db deleteCloud])
    {
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
//            [self.view addSubview:hud];
//            hud.labelText = @"已删除！";
//            hud.mode = MBProgressHUDModeText;
//            
//            [hud showAnimated:YES whileExecutingBlock:^{
//                sleep(2);
//            } completionBlock:^{
//                [hud removeFromSuperview];
//            }];
//            [self.lineManagerTableView reloadData];
//        });
    }
}
GLfloat colors[] = {
    1.0, 0.0, 0.0, 1.0,
};
#pragma mark 画线
-(void)drawLineWithPoint
{
    
    if (isDeleteUpdate) {
        return;
    }
    //    self.effect.texture2d0.enabled = NO;
    //混合纹理
    glDisable(GL_BLEND);
    
    GLKMatrixStackPush(self.textureMatrixStack);
    
    for (int i = 0; i<_lines.count; i++) {
        
        DrawPath *drawingPath = (DrawPath *)_lines[i];
        
        NSUInteger count = drawingPath.nodes.count;
        
        GLfloat vertices[count*2];
        
        for (int j =0; j<count; j++) {
            
            CGPoint thisPoint =[self convertGLPointWithPixPoint:[drawingPath.nodes[j]CGPointValue]];
            
            vertices[j*2] = thisPoint.x;
            vertices[j*2+1] = thisPoint.y;
            
        }
        
        self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
        [self.effect prepareToDraw];
        
        glBindBuffer(GL_ARRAY_BUFFER, _lineBuffer);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition,2,GL_FLOAT,GL_FALSE,2*4,vertices);
        
        glEnableVertexAttribArray(GLKVertexAttribColor);
        glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE,0,colors);
        
        // Set the line width
        glLineWidth(5.0);
        glDrawArrays(GL_LINE_STRIP, 0, (int)count);
        
        drawingPath = nil;
    }
    
    glDisableVertexAttribArray(GLKVertexAttribColor);
    GLKMatrixStackPop(self.textureMatrixStack);
    
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    
}
//画位图
-(void)drawCGImage
{
    
    self.effect.texture2d0.enabled = YES;
    glEnable(GL_BLEND);
    //设置纹理
    self.effect.texture2d0.name = _textureInfo.name;
    self.effect.texture2d0.target = _textureInfo.target;
    //把基本的模型矩阵（最先设置的）入栈
    GLKMatrixStackPush(self.textureMatrixStack);
    
    //glkMatrixStackgetmatrix4()函数从栈中获取矩阵
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    //开始绘制
    [self.effect prepareToDraw];
    glEnable(GL_BLEND);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _indexBuffer);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    //出栈
    GLKMatrixStackPop(self.textureMatrixStack);
    
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    
}
//画中心点
-(void)drawCenterPoint
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    [path addArcWithCenter:CGPointMake(Shitu_SCREENWIDTH/2, Shitu_SCREENHEIGHT/2) radius:2 startAngle:0 endAngle:2*M_PI clockwise:YES];
    
    //create shape layer
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = [UIColor redColor].CGColor;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.lineWidth = 1.5;
    shapeLayer.lineJoin = kCALineJoinRound;
    shapeLayer.lineCap = kCALineCapRound;
    shapeLayer.path = path.CGPath;
    //add it to our view
    [self.view.layer addSublayer:shapeLayer];
    path = nil;
    shapeLayer = nil;
}
#pragma mark touch回调
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _isInMoveMode = YES;
    _isFirst2Fingers = YES;
    
    UITouch * touch = [touches anyObject];
    _beginPoint = [self convertToGLFromScreenPoint:[touch locationInView:self.view]];
}
-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    _isInMoveMode = YES;
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    _isInMoveMode =YES;
    
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if([[event allTouches]count] > 1){
        _isInMoveMode = NO;
        if(_isFirst2Fingers){
            UITouch *touch1 = [[[event allTouches]allObjects]objectAtIndex:0];
            UITouch *touch2 = [[[event allTouches]allObjects]objectAtIndex:1];
            
            CGPoint touch1Point = [touch1 locationInView:self.view];
            CGPoint touch2Point = [touch2 locationInView:self.view];
            
            _initDistance = sqrtf((touch1Point.x - touch2Point.x) * (touch1Point.x - touch2Point.x) + (touch1Point.y - touch2Point.y) * (touch1Point.y - touch2Point.y));
            _isFirst2Fingers = NO;
            
            _initAngel = atan2f((touch1Point.y - touch2Point.y), (touch1Point.x - touch2Point.x));
            
            _scaleSave = _zoomScale;
            
            _angelSave = _rotateAngle;
            
            return;
        }
    }
    
    if(_isInMoveMode){
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:self.view];
        CGPoint previousPoint = [touch previousLocationInView:self.view];
        CGFloat screenDistanceX = (point.x - previousPoint.x);
        CGFloat screenDistanceY = -(point.y - previousPoint.y);
        _distanceX = [self convertToGLDistanceFromScreen:screenDistanceX];
        _distanceY = [self convertToGLDistanceFromScreen:screenDistanceY];
        
        _sumX+= _distanceX;
        _sumY+= _distanceY;
        return;
    }
    
    if(!_isInMoveMode&&[event allTouches].count >1){
        
        UITouch *touch1 = [[[event allTouches]allObjects]objectAtIndex:0];
        UITouch *touch2 = [[[event allTouches]allObjects]objectAtIndex:1];
        
        CGPoint touch1Point = [touch1 locationInView:self.view];
        CGPoint touch2Point = [touch2 locationInView:self.view];
        
        float detalAngel = atan2f((touch1Point.y - touch2Point.y), (touch1Point.x - touch2Point.x));
        
        _rotateAngle = _angelSave + detalAngel - _initAngel;
        
        float scaleAfterDistance = sqrtf((touch1Point.x - touch2Point.x) * (touch1Point.x - touch2Point.x) + (touch1Point.y - touch2Point.y) * (touch1Point.y - touch2Point.y));
        
        _zoomScale =  _scaleSave * scaleAfterDistance / _initDistance;
        return;
    }
    
}
#pragma mark 判断是否在范围内
-(BOOL)isBeyondBounds
{
    _transformedPoint = [self convertToTransformedPoint:_centerPoint];
    
    if (_transformedPoint.x <vertices[0]||_transformedPoint.x>vertices[2]||_transformedPoint.y<-1||_transformedPoint.y>1) {
        return YES;
    }
    else{
        return NO;
    }
    
}
#pragma mark 根据两点算矩形
-(CGPoint )unitVectorWithPointA:(CGPoint )pointA PointB:(CGPoint )pointB
{
    return CGPointMake(-(pointB.y - pointA.y)/sqrtf((pointA.x-pointB.x)*(pointA.x-pointB.x)+(pointA.y-pointB.y)*(pointA.y-pointB.y)), (pointB.x-pointA.x)/sqrtf((pointA.x-pointB.x)*(pointA.x-pointB.x)+(pointA.y-pointB.y)*(pointA.y-pointB.y)));
}

#pragma mark 坐标转化
-(CGPoint)convertToTransformedPoint:(CGPoint)point
{
    CGPoint traslatedPoint = CGPointMake(point.x - _sumX, point.y - _sumY);
    CGPoint zoomedPoint = CGPointMake(traslatedPoint.x/_zoomScale, traslatedPoint.y/_zoomScale);
    CGPoint rotatedPoint = CGPointMake(zoomedPoint.x*cosf(_rotateAngle)-sinf(_rotateAngle)*zoomedPoint.y, zoomedPoint.y*cosf(_rotateAngle)+zoomedPoint.x*sin(_rotateAngle));
    return rotatedPoint;
}
-(CGSize )convertToGLSizeFromScreenSize:(CGSize )size
{
    CGSize GLSize  = CGSizeMake(Shitu_SCREENWIDTH*2/Shitu_SCREENHEIGHT, 2);
    
    return GLSize;
}

-(CGFloat )scaleBetweenGLAndScreen
{
    return 2/Shitu_SCREENHEIGHT;
}

-(CGPoint )convertToGLFromScreenPoint:(CGPoint )point
{
    CGPoint GLpoint = CGPointMake(point.x-Shitu_SCREENWIDTH/2, Shitu_SCREENHEIGHT/2 - point.y);
    CGFloat scale = [self scaleBetweenGLAndScreen];
    GLpoint = CGPointMake(scale*GLpoint.x,  scale*GLpoint.y);
    return GLpoint;
}
-(CGFloat )convertToGLDistanceFromScreen:(CGFloat)offSet
{
    CGFloat scale = [self scaleBetweenGLAndScreen];
    
    return offSet*scale;
}
-(CGFloat)distanceBetweenPointA:(CGPoint )pointA andPointB:(CGPoint)pointB
{
    return sqrtf((pointA.x - pointB.x)*(pointA.x - pointB.x) + (pointA.y - pointB.y)*(pointA.y - pointB.y));
}
-(CGPoint )convertToScreenPointWithGLPoint:(CGPoint )GLPoint
{
    CGFloat scale = [self scaleBetweenGLAndScreen];
    GLPoint = CGPointMake(GLPoint.x/scale,  GLPoint.y/scale);
    CGPoint point = CGPointMake(GLPoint.x+Shitu_SCREENWIDTH/2, Shitu_SCREENHEIGHT/2 - GLPoint.y);
    return point;
}
-(CGPoint)convertPixPointWithGLPoint:(CGPoint)GLPoint
{
    GLPoint =  CGPointMake([self convertToScreenPointWithGLPoint:GLPoint].x, [self convertToScreenPointWithGLPoint:GLPoint].y);
    CGPoint point = CGPointMake((GLPoint.x -(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT, GLPoint.y*_imageSize.height/Shitu_SCREENHEIGHT);
    return point;
}
-(CGPoint )convertGLPointWithPixPoint:(CGPoint)GLPoint
{
    GLPoint = CGPointMake(GLPoint.x*Shitu_SCREENHEIGHT/_imageSize.height+(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2, GLPoint.y*Shitu_SCREENHEIGHT/_imageSize.height);
    CGPoint point = CGPointMake([self convertToGLFromScreenPoint:GLPoint].x, [self convertToGLFromScreenPoint:GLPoint].y);
    return point;
}





#pragma mark 精度降低
//-(void)magnetometerFromDeviceMotionUnreliable
//{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:glkView];
//        [self.view addSubview:hud];
//        hud.labelText = @"精度降低";
//        hud.mode = MBProgressHUDModeText;
//        
//        [hud showAnimated:YES whileExecutingBlock:^{
//            sleep(1);
//        } completionBlock:^{
//            [hud removeFromSuperview];
//        }];
//    });
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)popToRootViewController
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_sensor unregisterSensors];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
