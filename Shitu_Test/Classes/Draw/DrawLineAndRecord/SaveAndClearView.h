//
//  SaveAndClearView.h
//  Shitu_Test
//
//  Created by publicshitu on 15/6/14.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaveAndClearView : UIView
@property(nonatomic,strong)UIButton *clearLocal;
@property(nonatomic,strong)UIButton *clearArea;
@property(nonatomic,strong)UIButton *clearCloud;
@end
