//
//  PathBuilderView.m
//  Shitu_Test
//
//  Created by publicshitu on 15/5/11.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//


#import "PathBuilderView.h"
@implementation PathBuilderView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _drawLine = [UIButton buttonWithType:UIButtonTypeSystem];
        _drawLine.frame = CGRectMake(46, 25, 65, 65);
        [_drawLine setBackgroundImage:[UIImage imageNamed:@"drawline_btn"] forState:UIControlStateNormal];
        [_drawLine setBackgroundImage:[UIImage imageNamed:@"drawline_btn_pressed_unable"] forState:UIControlStateDisabled];
        [self addSubview:_drawLine];
        
        _record = [UIButton buttonWithType:UIButtonTypeSystem];
        _record.frame = CGRectMake(Shitu_SCREENWIDTH/2-150/4, 15 ,75, 76);
        [_record setBackgroundImage:[UIImage imageNamed:@"record_btn"] forState:UIControlStateNormal];
        [_record setBackgroundImage:[UIImage imageNamed:@"record_btn_unable"] forState:UIControlStateDisabled];
        [_record setBackgroundImage:[UIImage imageNamed:@"record_btn_process_l"] forState:UIControlStateSelected];
        [self addSubview:_record];
        
        _clean = [UIButton buttonWithType:UIButtonTypeSystem];
        _clean.frame = CGRectMake(Shitu_SCREENWIDTH-46-60, 25, 65, 65);
        [_clean setBackgroundImage:[UIImage imageNamed:@"clear_btn"] forState:UIControlStateNormal];
        [_clean setBackgroundImage:[UIImage imageNamed:@"clear_btn_unable"] forState:UIControlStateDisabled];
        [self addSubview:_clean];
        
    }
    return self;
}
@end
