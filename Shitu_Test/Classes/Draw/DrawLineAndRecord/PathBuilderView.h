//
//  PathBuilderView.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/11.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PathBuilderView : UIView
@property(nonatomic,strong)UIButton *drawLine;
@property(nonatomic,strong)UIButton *record;
@property(nonatomic,strong)UIButton *clean;
@end
