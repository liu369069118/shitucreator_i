//
//  DrawPath.h
//  GL
//
//  Created by publicshitu on 15/5/29.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
@interface DrawPath : NSObject
@property(nonatomic,strong)NSMutableArray *nodes;
@property(nonatomic,assign)int whichLine;
@end
