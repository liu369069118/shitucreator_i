//
//  LocationViewController.h
//  Shitu_Test
//
//  Created by shitupublic on 15/6/24.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "SHTSensors.h"
#import "StoreModel.h"
#import "SHTLocationCloud.h"

@interface LocationViewController : GLKViewController<SHTLocationRespondeDelegate,SHTLocationCloudDelegate>

@property(nonatomic,strong)StoreModel *currentStore;

@property(nonatomic,assign)int whichArea;

@property(nonatomic,assign)int floorIndex;

@property(nonatomic,copy)NSString *path;

@property(nonatomic)GLKMatrixStackRef textureMatrixStack;
@end
