//
//  SaveAndClearView.m
//  Shitu_Test
//
//  Created by publicshitu on 15/6/14.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "SaveAndClearView.h"

@implementation SaveAndClearView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
-(instancetype)initWithFrame:(CGRect)frame
{
    if (self) {
        self = [super initWithFrame:frame];
        
         _clearLocal = [UIButton buttonWithType:UIButtonTypeSystem];
        _clearLocal.frame = CGRectMake(0, 0, self.frame.size.width, 40);
        [_clearLocal setTitle:@"清除本地数据" forState:UIControlStateNormal];
        [_clearLocal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
         _clearArea = [UIButton buttonWithType:UIButtonTypeSystem];
        _clearArea.frame = CGRectMake(0, 40, self.frame.size.width, 40);
        [_clearArea setTitle:@"清除本层数据" forState:UIControlStateNormal];
        [_clearArea setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        _clearCloud= [UIButton buttonWithType:UIButtonTypeSystem];
        _clearCloud.frame = CGRectMake(0, 80, self.frame.size.width, 40);
        [_clearCloud setTitle:@"清除云端数据" forState:UIControlStateNormal];
        [_clearCloud setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
        [self addSubview:_clearArea];
        [self addSubview:_clearCloud];
        [self addSubview:_clearLocal];
    }
    return self;
}


@end
