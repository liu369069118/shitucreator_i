//
//  openGLViewController.m
//  GL
//
//  Created by publicshitu on 15/5/20.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//


#import "LocationViewController.h"

#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <GLKit/GLKTextureLoader.h>
#import "NSString+URL.h"
#import "FloorModel.h"
#import "SDWebImageManager.h"
#import "SHTLocationCloud.h"
#import "SHTNatureParameters.h"
#import "SHTPosition.h"

@interface LocationViewController ()<UIAlertViewDelegate>
{
    SHTLocationCloud * _location;
    
    volatile float _screenAspect;
    
    BOOL _isInMoveMode;
    
    BOOL _isFirst2Fingers;
    
    BOOL _isBarItemSelected;
    
    BOOL _ischangeAreaSelected;
    
    volatile float _rotateAngle;
    
    float _compassAngel;
    
    float _initDistance;
    
    float _initAngel;
    
    float _scaleSave;
    
    float _angelSave;
    
    volatile float _sumX;
    
    volatile float _sumY;
    
    volatile float _zoomScale;
    
    GLuint _vertexBuffer;
    GLuint _indexBuffer;
    
    GLKView *glkView;
    
}
@property(nonatomic,strong)EAGLContext *context;
@property(strong,nonatomic)GLKBaseEffect *effect;
@property(nonatomic,assign)CGPoint beginPoint;

@property(nonatomic,assign)CGPoint centerPoint;
@property(nonatomic,assign)CGSize imageSize;
@property(nonatomic,assign)CGFloat distanceX;
@property(nonatomic,assign)CGFloat distanceY;
//缩放
@property(nonatomic,strong)NSMutableArray *touchs;

@property(nonatomic,strong)GLKTextureInfo *textureInfo;
@property(nonatomic,strong)GLKTextureInfo *locationTextureInfo;
@property(nonatomic,strong)GLKTextureInfo *compassTextureInfo;

@property(nonatomic,assign)CGFloat distanceOfTwoPoint;

@property(nonatomic,assign)CGPoint locationPoint;
@property(nonatomic,assign)CGPoint walkPoint;

@property(nonatomic,assign)CGFloat locationAngel;
@property(nonatomic,strong)UILabel *exceptionLabel;

@property(nonatomic,strong)UIImage *mapImage;
@end

@implementation LocationViewController
static GLfloat texCoords[]={
    0,0,
    1,0,
    0,1,
    1,1,
    
};
static GLfloat vertices[] = {
    -1, -1,//左下
    1, -1,//右下
    1, 1,//右上
    -1, 1,//左上
};
static GLfloat locationVertices[] = {
    -1,-1,
    1,-1,
    1,1,
    -1,1,
};
static GLfloat compassVertices[] = {
    -1,-1,
    1,-1,
    1,1,
    -1,1,
};

static GLfloat circleVertices[720];

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"定位测试";
    FloorModel *floor = _currentStore.floors[_floorIndex];
    NSData *data = [NSData dataWithContentsOfFile:_path];
    self.mapImage  = [UIImage imageWithData:data];
    
    //54.223.143.190
    SHTNatureParameters * parameters = [[SHTNatureParameters alloc]initWithStoreId:(int)_currentStore.storeID Host:@"54.223.143.190" Port:10001 USEOneWayMatch:NO WithInitFloor:_whichArea InitX:-1 InitY:-1 AndScale:floor.scale MapWidth:self.mapImage.size.width AndHeight:self.mapImage.size.height AndAngleOffset:0];
    
    _location = [[SHTLocationCloud alloc]initWithNatureParameters:parameters];
    _location.delegate = self;
    _location.netDelegate = self;
    [_location start];
    
    //glkView
    [self prepareGLKView];
    
    
}

- (void)getLocation:(SHTPosition *)position
{
    _locationPoint.x = position.x;
    _locationPoint.y =  position.y;

    [self startLocation:nil];
    NSLog(@"positon = (%f,%f)",position.x,position.y);
}

- (void)onStopWalk
{
    
}

- (void)onWalk
{

}

- (void)getAngle:(int)angle
{
    
}

- (void)getDebugInfo:(NSString *)debug
{
    
}

- (void)getFloor:(int)floor
{
    
}

- (void)getRadius:(int)radius
{
    
}

- (void)onError:(NSError *)error
{
    
}

- (void)getStatus:(int)status Message:(NSString *)message
{
    _exceptionLabel.text = message;
}


- (void)onNetProblem:(NSException *)exception
{
    NSLog(@">>>>>>>>>>net problems");
    dispatch_sync(dispatch_get_main_queue(), ^{
        //Update UI in UI thread here
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"服务器连接异常" message:@"请重新操作" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.delegate = self;
        [alert show];
        alert = nil;
    });
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark glkView
-(void)prepareGLKView
{
    
    self.view.userInteractionEnabled = YES;
    _zoomScale = 1;
    _scaleSave = 1;
    _angelSave = 0;
    self.touchs = [NSMutableArray array];
    
    _ischangeAreaSelected = NO;
    //context
    
    self.context = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (!self.context) {
        NSLog(@"failed to creat ES context");
    }
    else if (![EAGLContext setCurrentContext:self.context])
    {
        NSLog(@"failed to set ES context current");
    }
    //GLKView
    glkView = [[GLKView alloc]initWithFrame:[UIScreen mainScreen].bounds context:self.context];
    self.view = glkView;
    //定位button
    UIButton *locationBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    locationBtn.frame = CGRectMake(10, Shitu_SCREENHEIGHT-70, 40, 40);
    locationBtn.backgroundColor = [UIColor whiteColor];
    [locationBtn setBackgroundImage:[UIImage imageNamed:@"home_positioning"] forState:UIControlStateNormal];
    [locationBtn addTarget:self  action:@selector(startLocation:) forControlEvents:UIControlEventTouchUpInside];
//    [glkView addSubview:locationBtn];
    
    UIButton *changeArea = [UIButton buttonWithType:UIButtonTypeSystem];
    changeArea.frame = CGRectMake(10, Shitu_SCREENHEIGHT-120, 40, 40);
    changeArea.backgroundColor = [UIColor whiteColor];
    changeArea.tag = 700;
    [changeArea setBackgroundImage:[UIImage imageNamed:@"rect"] forState:UIControlStateNormal];
    [changeArea setTitle:[NSString stringWithFormat:@"%d",_whichArea] forState:UIControlStateNormal];
    [changeArea addTarget:self action:@selector(changeArea:) forControlEvents:UIControlEventTouchUpInside];
//    [glkView addSubview:changeArea];
    
    _exceptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 64, Shitu_SCREENWIDTH, 30)];
    _exceptionLabel.backgroundColor = [UIColor redColor];
    _exceptionLabel.alpha = 0.6;
    _exceptionLabel.font = [UIFont systemFontOfSize:12];
    _exceptionLabel.textColor = [UIColor whiteColor];
//    [glkView addSubview:_exceptionLabel];
    
    [EAGLContext setCurrentContext:self.context];
    //设置帧数
    self.preferredFramesPerSecond = 60;
    
    glEnable(GL_DEPTH_TEST);
    
    self.effect = [[GLKBaseEffect alloc]init];
    //创建矩阵栈
    self.textureMatrixStack = GLKMatrixStackCreate(kCFAllocatorDefault);
    
    CGSize size = self.view.bounds.size;
    _screenAspect = fabs(size.width/size.height);
    //底图
    self.textureInfo = [self loadImage:_mapImage];
    //定位点图
    UIImage *location = [UIImage imageNamed:@"icon_center_point"];
    locationVertices[0] = [self convertToGLDistanceFromScreen: -location.size.width/2];
    locationVertices[1] =[self convertToGLDistanceFromScreen: -location.size.height/2];
    locationVertices[2] = [self convertToGLDistanceFromScreen: location.size.width/2];
    locationVertices[3] = [self  convertToGLDistanceFromScreen: -location.size.height/2];
    locationVertices[4] = [self convertToGLDistanceFromScreen: -location.size.width/2];
    locationVertices[5] = [self convertToGLDistanceFromScreen: location.size.height/2];
    locationVertices[6] = [self convertToGLDistanceFromScreen: location.size.width/2];
    locationVertices[7] = [self convertToGLDistanceFromScreen: location.size.height/2];
    _locationTextureInfo = [self loadImage:location];
    //指南针图
    UIImage *compass = [UIImage imageNamed:@"compass"];
    compassVertices[0] = [self convertToGLDistanceFromScreen: -compass.size.width/2];
    compassVertices[1] =[self convertToGLDistanceFromScreen: -compass.size.height/2];
    compassVertices[2] = [self convertToGLDistanceFromScreen: compass.size.width/2];
    compassVertices[3] = [self  convertToGLDistanceFromScreen: -compass.size.height/2];
    compassVertices[4] = [self convertToGLDistanceFromScreen: -compass.size.width/2];
    compassVertices[5] = [self convertToGLDistanceFromScreen: compass.size.height/2];
    compassVertices[6] = [self convertToGLDistanceFromScreen: compass.size.width/2];
    compassVertices[7] = [self convertToGLDistanceFromScreen: compass.size.height/2];
    //初始化栈
    GLKMatrixStackLoadMatrix4(self.textureMatrixStack, self.effect.transform.modelviewMatrix);
    
    GLKMatrix4 pro = GLKMatrix4Identity;
    
    pro = GLKMatrix4Scale(pro, 1.0f/_screenAspect, 1.0f, 1.0f);
    self.effect.transform.projectionMatrix = pro;
    
    _isInMoveMode = YES;
    
    vertices[0] = -_mapImage.size.width/_mapImage.size.height;
    vertices[2] = _mapImage.size.width/_mapImage.size.height;
    vertices[4] = -_mapImage.size.width/_mapImage.size.height;
    vertices[6] = _mapImage.size.width/_mapImage.size.height;
    _imageSize = CGSizeMake(_mapImage.size.width, _mapImage.size.height);
    
    
}
#pragma mark 定位触发事件
-(void)startLocation:(UIButton *)sender
{
    _sumX = -_walkPoint.x;
    _sumY = -_walkPoint.y;
//    [self translateAnimationFromPointA:CGPointMake(_sumX, _sumY) toPointB:CGPointMake(-_walkPoint.x, -_walkPoint.y)];
}
//绘制定位点
-(void)drawLocatonPoint
{
    self.effect.texture2d0.enabled = YES;
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    self.effect.texture2d0.name = _locationTextureInfo.name;
    self.effect.texture2d0.target = _locationTextureInfo.target;
    
    GLKMatrixStackPush(self.textureMatrixStack);
    
    _walkPoint = [self convertGLPointWithPixPoint:CGPointMake(_locationPoint.x, _locationPoint.y)];
    //对入站矩阵进行操作
    GLKMatrixStackTranslate(self.textureMatrixStack,(GLfloat) _walkPoint.x , (GLfloat) _walkPoint.y, -1.0f);

    GLKMatrixStackRotate(self.textureMatrixStack, -_rotateAngle, 0.0f, 0.0f, 1.0f);
    
    //glkmatrixStackgetmatrix4()函数从栈中获取矩阵
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    //开始绘制
    [self.effect prepareToDraw];
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _indexBuffer);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, locationVertices);
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    //出栈
    GLKMatrixStackPop(self.textureMatrixStack);
    
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    
}

//贴位图
-(GLKTextureInfo *)loadImage:(UIImage *)image{
    
    GLKTextureInfo *info;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],GLKTextureLoaderOriginBottomLeft, nil];
    return info = [GLKTextureLoader textureWithCGImage:image.CGImage options:options error:nil];
}
-(void)update
{
    [self startLocation:nil];
}
#pragma mark glkView监听
-(void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    
    glClearColor(0.5, 0.5, 0.5, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    GLKMatrixStackPush(self.textureMatrixStack);
    
    GLKMatrixStackTranslate(self.textureMatrixStack, _sumX, _sumY, 0);
    GLKMatrixStackScale(self.textureMatrixStack, _zoomScale, _zoomScale, 0.0f);
    GLKMatrixStackRotate(self.textureMatrixStack, -_rotateAngle, 0.0f, 0.0f, 1.0f);
    
    [self drawCGImage];
    [self drawLocatonPoint];
    //    [self drawCircle];
    
    GLKMatrixStackPop(self.textureMatrixStack);
}


//画位图
-(void)drawCGImage
{
    self.effect.texture2d0.enabled = YES;
    //设置纹理
    self.effect.texture2d0.name = _textureInfo.name;
    self.effect.texture2d0.target = _textureInfo.target;
    //把基本的模型矩阵（最先设置的）入栈
    GLKMatrixStackPush(self.textureMatrixStack);
    
    //glkMatrixStackgetmatrix4()函数从栈中获取矩阵
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    //开始绘制
    [self.effect prepareToDraw];
    glEnable(GL_BLEND);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _indexBuffer);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    //出栈
    GLKMatrixStackPop(self.textureMatrixStack);
    
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    
}
-(void)drawCompass
{
    self.effect.texture2d0.enabled = YES;
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //设置纹理
    self.effect.texture2d0.name = _compassTextureInfo.name;
    self.effect.texture2d0.target = _compassTextureInfo.target;
    GLKMatrixStackPush(self.textureMatrixStack);
    
    //对入站矩阵进行操作
    GLKMatrixStackRotate(self.textureMatrixStack, -_compassAngel, 0.0f, 0.0f, 1.0f);
    
    //glkmatrixStackgetmatrix4()函数从栈中获取矩阵
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    //开始绘制
    [self.effect prepareToDraw];
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _indexBuffer);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 0, compassVertices);
    
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    //出栈
    GLKMatrixStackPop(self.textureMatrixStack);
    
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    
}
-(void)drawCircle
{
    self.effect.texture2d0.enabled = NO;
    glDisable(GL_BLEND);
    
    GLKMatrixStackPush(self.textureMatrixStack);
    
    //对入站矩阵进行操作
    
    //glkmatrixStackgetmatrix4()函数从栈中获取矩阵
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    //开始绘制
    [self.effect prepareToDraw];
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _indexBuffer);
    GLfloat colors[] = {
        0.0f, 0.0f, 0.0f,
    };
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glEnableVertexAttribArray(GLKVertexAttribColor);
    glVertexAttribPointer(GLKVertexAttribColor,4, GL_FLOAT, GL_FALSE, 2*4, colors);
    glVertexAttribPointer(GLKVertexAttribPosition,2,GL_FLOAT,GL_FALSE,2*4,circleVertices);
    // Set the line width
    glDrawArrays(GL_TRIANGLE_FAN, 0, 360);
    
    //出栈
    GLKMatrixStackPop(self.textureMatrixStack);
    
    self.effect.transform.modelviewMatrix = GLKMatrixStackGetMatrix4(self.textureMatrixStack);
    
    
}
#pragma mark 左侧button点击事件
-(void)changeArea:(UIButton *)sender
{
    _ischangeAreaSelected = !_ischangeAreaSelected;
    if (_ischangeAreaSelected == YES) {
        
        for (int i=0; i<_currentStore.floors.count; i++) {
            [sender setTitle:@"收回" forState:UIControlStateNormal];
            
            UIButton *areaBtn = [UIButton buttonWithType:UIButtonTypeSystem];
            areaBtn.frame = CGRectMake(10, Shitu_SCREENHEIGHT-120-50*(i+1), 40, 40);
            areaBtn.backgroundColor = [UIColor whiteColor];
            [areaBtn setBackgroundImage:[UIImage imageNamed:@"rect"] forState:UIControlStateNormal];
            FloorModel *floor = _currentStore.floors[i];
            [areaBtn setTitle:floor.floor_name forState:UIControlStateNormal];
            areaBtn.tag = 1000 +i;
            [areaBtn addTarget:self  action:@selector(certainArea:) forControlEvents:UIControlEventTouchUpInside];
            [glkView addSubview:areaBtn];
            
        }
    }
    else
    {
        for (int i=0; i<_currentStore.floors.count; i++) {
            [sender setTitle:[NSString stringWithFormat:@"%d",_whichArea] forState:UIControlStateNormal];
            UIView *view = [glkView viewWithTag:1000+i];
            [view removeFromSuperview];
        }
    }
    
}
-(void)certainArea:(UIButton *)sender
{
    NSLog(@"buttonIndex=%zi", sender.tag);
    FloorModel *floor = _currentStore.floors[sender.tag-1000];
    
    SDWebImageManager *webImagemanager =  [SDWebImageManager sharedManager];
    [webImagemanager downloadImageWithURL:[NSURL URLWithString:[floor.map_url URLEncodedString]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        NSLog(@"进度：%zi",receivedSize*100/(receivedSize+expectedSize));
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
        self.textureInfo = [self loadImage:image];
        vertices[0] = -image.size.width/image.size.height;
        vertices[2] = image.size.width/image.size.height;
        vertices[4] = -image.size.width/image.size.height;
        vertices[6] = image.size.width/image.size.height;
    }];
    
}
#pragma mark touch回调
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _isInMoveMode = YES;
    _isFirst2Fingers = YES;
    
    UITouch * touch = [touches anyObject];
    _beginPoint = [self convertToGLFromScreenPoint:[touch locationInView:self.view]];
}
-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    _isInMoveMode = YES;
    
    if([[event allTouches]count] > 1){
        _isInMoveMode = NO;
        if(_isFirst2Fingers){
            UITouch *touch1 = [[[event allTouches]allObjects]objectAtIndex:0];
            UITouch *touch2 = [[[event allTouches]allObjects]objectAtIndex:1];
            
            CGPoint touch1Point = [touch1 locationInView:self.view];
            CGPoint touch2Point = [touch2 locationInView:self.view];
            
            _initDistance = sqrtf((touch1Point.x - touch2Point.x) * (touch1Point.x - touch2Point.x) + (touch1Point.y - touch2Point.y) * (touch1Point.y - touch2Point.y));
            _isFirst2Fingers = NO;
            
            _initAngel = atan2f((touch1Point.y - touch2Point.y), (touch1Point.x - touch2Point.x));
            
            _scaleSave = _zoomScale;
            
            _angelSave = _rotateAngle;
            
            return;
        }
    }
    
    if(_isInMoveMode){
        if ([self isBeyondBounds]) {

        }
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:self.view];
        CGPoint previousPoint = [touch previousLocationInView:self.view];
        
        CGFloat screenDistanceX = (point.x - previousPoint.x);
        CGFloat screenDistanceY = -(point.y - previousPoint.y);
        
        _distanceX = [self convertToGLDistanceFromScreen:screenDistanceX];
        _distanceY = [self convertToGLDistanceFromScreen:screenDistanceY];
        
        _sumX+= _distanceX;
        _sumY+= _distanceY;
        return;
    }
    
    if(!_isInMoveMode){
        UITouch *touch1 = [[[event allTouches]allObjects]objectAtIndex:0];
        UITouch *touch2 = [[[event allTouches]allObjects]objectAtIndex:1];
        
        CGPoint touch1Point = [touch1 locationInView:self.view];
        CGPoint touch2Point = [touch2 locationInView:self.view];
        
        float detalAngel = atan2f((touch1Point.y - touch2Point.y), (touch1Point.x - touch2Point.x));
        
        _rotateAngle = _angelSave + detalAngel - _initAngel;
        
        float scaleAfterDistance = sqrtf((touch1Point.x - touch2Point.x) * (touch1Point.x - touch2Point.x) + (touch1Point.y - touch2Point.y) * (touch1Point.y - touch2Point.y));
        
        _zoomScale =  _scaleSave * scaleAfterDistance / _initDistance;
        return;
    }
    
}
#pragma mark isbeyondFrame
-(BOOL)isbeyondBounds:(CGPoint )point
{
    CGPoint transformedPoint = [self convertToTransformedPoint:point];
    
    if (transformedPoint.x <vertices[0]||transformedPoint.x>vertices[2]||transformedPoint.y<-1||transformedPoint.y>1) {
        return YES;
    }
    else{
        return NO;
    }

}
-(BOOL)isBeyondBounds
{
    CGPoint transformedPoint = [self convertToTransformedPoint:_centerPoint];
    
    if (transformedPoint.x <vertices[0]||transformedPoint.x>vertices[2]||transformedPoint.y<-1||transformedPoint.y>1) {
        return YES;
    }
    else{
        return NO;
    }
    
}
-(void)translateAnimationFromPointA:(CGPoint )pointA toPointB:(CGPoint )pointB
{
    
    CABasicAnimation *anima=[CABasicAnimation animation];
    
    anima.keyPath=@"position";
    anima.fromValue=[NSValue valueWithCGPoint:pointA];
    anima.toValue=[NSValue valueWithCGPoint:pointB];
    
    anima.removedOnCompletion=NO;
    
    anima.fillMode=kCAFillModeForwards;
    
    [glkView.layer addAnimation:anima forKey:nil];
    
}
#pragma mark 坐标转化
-(CGPoint)convertToTransformedPoint:(CGPoint)point
{
    CGPoint traslatedPoint = CGPointMake(point.x - _sumX, point.y - _sumY);
    CGPoint zoomedPoint = CGPointMake(traslatedPoint.x/_zoomScale, traslatedPoint.y/_zoomScale);
    CGPoint rotatedPoint = CGPointMake(zoomedPoint.x*cosf(_rotateAngle)-sinf(_rotateAngle)*zoomedPoint.y, zoomedPoint.y*cosf(_rotateAngle)+zoomedPoint.x*sin(_rotateAngle));
    return rotatedPoint;
}
-(CGSize )convertToGLSizeFromScreenSize:(CGSize )size
{
    CGSize GLSize  = CGSizeMake(Shitu_SCREENWIDTH*2/Shitu_SCREENHEIGHT, 2);
    
    return GLSize;
}

-(CGFloat )scaleBetweenGLAndScreen
{
    return 2/Shitu_SCREENHEIGHT;
}

-(CGPoint )convertToGLFromScreenPoint:(CGPoint )point
{
    CGPoint GLpoint = CGPointMake(point.x-Shitu_SCREENWIDTH/2, Shitu_SCREENHEIGHT/2 - point.y);
    CGFloat scale = [self scaleBetweenGLAndScreen];
    GLpoint = CGPointMake(scale*GLpoint.x,  scale*GLpoint.y);
    return GLpoint;
}
-(CGFloat )convertToGLDistanceFromScreen:(CGFloat)offSet
{
    CGFloat scale = [self scaleBetweenGLAndScreen];
    
    return offSet*scale;
}
-(CGFloat)distanceBetweenPointA:(CGPoint )pointA andPointB:(CGPoint)pointB
{
    return sqrtf((pointA.x - pointB.x)*(pointA.x - pointB.x) + (pointA.y - pointB.y)*(pointA.y - pointB.y));
}
-(CGPoint )convertToScreenPointWithGLPoint:(CGPoint )GLPoint
{
    CGFloat scale = [self scaleBetweenGLAndScreen];
    GLPoint = CGPointMake(GLPoint.x/scale,  GLPoint.y/scale);
    CGPoint point = CGPointMake(GLPoint.x+Shitu_SCREENWIDTH/2, Shitu_SCREENHEIGHT/2 - GLPoint.y);
    return point;
}
-(CGPoint)convertPixPointWithGLPoint:(CGPoint)GLPoint
{
    GLPoint =  CGPointMake([self convertToScreenPointWithGLPoint:GLPoint].x, [self convertToScreenPointWithGLPoint:GLPoint].y);
    CGPoint point = CGPointMake((GLPoint.x -(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2)*_imageSize.height/Shitu_SCREENHEIGHT, GLPoint.y*_imageSize.height/Shitu_SCREENHEIGHT);
    return point;
}
-(CGPoint )convertGLPointWithPixPoint:(CGPoint)GLPoint
{
    GLPoint = CGPointMake(GLPoint.x*Shitu_SCREENHEIGHT/_imageSize.height+(Shitu_SCREENWIDTH - _imageSize.width*Shitu_SCREENHEIGHT/_imageSize.height)/2, GLPoint.y*Shitu_SCREENHEIGHT/_imageSize.height);
    CGPoint point = CGPointMake([self convertToGLFromScreenPoint:GLPoint].x, [self convertToGLFromScreenPoint:GLPoint].y);
    return point;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_location stop];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
