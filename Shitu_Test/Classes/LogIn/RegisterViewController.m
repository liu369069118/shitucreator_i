//
//  RegisterViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/4/29.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "RegisterViewController.h"
@interface RegisterViewController ()
@property(strong,nonatomic)UIWebView *registerView;
@end

@implementation RegisterViewController
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *registerURL = [NSURL URLWithString:REGISTERURL];
    NSURLRequest *registerRequest = [NSURLRequest requestWithURL:registerURL];
    self.registerView = [[UIWebView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:_registerView];
    _registerView.delegate = self;
    
    [_registerView loadRequest:registerRequest];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
