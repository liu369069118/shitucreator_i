//
//  RegistViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "RegistViewController.h"
#import "MBProgressHUD.h"
#import "AccountTokenManager.h"
#import "AFHTTPRequestOperationManager.h"
@interface RegistViewController ()
@property(nonatomic,strong)UILabel *welcome;
@property(nonatomic,strong)UITextField *account;
@property(nonatomic,strong)UITextField *email;
@property(nonatomic,strong)UITextField *password;
@property(nonatomic,strong)UITextField *passwordAgain;
@property(nonatomic,strong)UIButton *creatAccount;
@property(nonatomic,strong)UIButton *login;
@property(nonatomic,assign)BOOL isTextOffset;
@end

@implementation RegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //背景
    UIImageView *background = [[UIImageView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    background.image = [UIImage imageNamed:@"login_BG"];
    [self.view addSubview:background];
    //欢迎
    UIImageView *title = [[UIImageView alloc]initWithFrame:CGRectMake(38*Shitu_SCREENWIDTH/375, 121*Shitu_SCREENWIDTH/375, 300*Shitu_SCREENWIDTH/375, 50*Shitu_SCREENWIDTH/375)];
    title.image = [UIImage imageNamed:@"login_welcome"];
    [self.view addSubview:title];
    //用户名
    UIView *textback1View = [[UIView alloc]initWithFrame:CGRectMake(40*Shitu_SCREENWIDTH/375, 257*Shitu_SCREENHEIGHT/667, (590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    textback1View.backgroundColor = Shitu_ColorWithRgbValue(0xf0f0f0);
    [self.view addSubview:textback1View];
    
    UIImageView *accountIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10*Shitu_SCREENWIDTH/375,10*Shitu_SCREENHEIGHT/667, 29, 29)];
    accountIcon.image = [UIImage imageNamed:@"register_name_pic"];
    [textback1View addSubview:accountIcon];
    
    self.account = [[UITextField alloc]initWithFrame:CGRectMake(39*Shitu_SCREENWIDTH/375, 0, (590/2-44)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    _account.clearButtonMode = UITextFieldViewModeUnlessEditing;
    _account.backgroundColor = [UIColor clearColor];
    _account.delegate = self;
    _account.placeholder = @" 用户名";
    [textback1View addSubview:_account];
    //电子邮箱
    UIView *textback2View = [[UIView alloc]initWithFrame:CGRectMake(40*Shitu_SCREENWIDTH/375, (257+53)*Shitu_SCREENHEIGHT/667, (590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    textback2View.backgroundColor = Shitu_ColorWithRgbValue(0xf0f0f0);
    [self.view addSubview:textback2View];
    
    UIImageView *emailIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10*Shitu_SCREENWIDTH/375,10*Shitu_SCREENHEIGHT/667, 29, 29)];
    [self.view addSubview:emailIcon];
    emailIcon.image = [UIImage imageNamed:@"login_email_pic"];
    [textback2View addSubview:emailIcon];
    
    self.email = [[UITextField alloc]initWithFrame:CGRectMake(39*Shitu_SCREENWIDTH/375,0, (590/2-44)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    self.email.backgroundColor = [UIColor clearColor];
    _email.delegate = self;
    _email.clearButtonMode = UITextFieldViewModeUnlessEditing;
    _email.placeholder = @" 电子邮箱";
    [textback2View addSubview:_email];
    //密码
    UIView *textback3View = [[UIView alloc]initWithFrame:CGRectMake(40*Shitu_SCREENWIDTH/375, (257+106)*Shitu_SCREENHEIGHT/667, (590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    textback3View.backgroundColor = Shitu_ColorWithRgbValue(0xf0f0f0);
    [self.view addSubview:textback3View];
    
    UIImageView *pwdIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10*Shitu_SCREENWIDTH/375,10*Shitu_SCREENHEIGHT/667, 29, 29)];
    [self.view addSubview:pwdIcon];
    pwdIcon.image = [UIImage imageNamed:@"login_pwd_pic"];
    [textback3View addSubview:pwdIcon];
    
    self.password = [[UITextField alloc]initWithFrame:CGRectMake(39*Shitu_SCREENWIDTH/375, 0, (590/2-44)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    self.password.backgroundColor = [UIColor clearColor];
    _password.delegate = self;
    _password.secureTextEntry = YES;
    _password.clearButtonMode = UITextFieldViewModeUnlessEditing;
    _password.placeholder = @" 密码";
    [textback3View addSubview:_password];
    
    //再次输入密码
    UIView *textback4View = [[UIView alloc]initWithFrame:CGRectMake(40*Shitu_SCREENWIDTH/375, (257+159)*Shitu_SCREENHEIGHT/667, (590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    textback4View.backgroundColor = Shitu_ColorWithRgbValue(0xf0f0f0);
    [self.view addSubview:textback4View];
    
    UIImageView *pwdAIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10*Shitu_SCREENWIDTH/375,10*Shitu_SCREENHEIGHT/667, 29, 29)];
    [self.view addSubview:pwdAIcon];
    pwdAIcon.image = [UIImage imageNamed:@"register_pwd_pic"];
    [textback4View addSubview:pwdAIcon];
    
    self.passwordAgain = [[UITextField alloc]initWithFrame:CGRectMake(39*Shitu_SCREENWIDTH/375,0, (590/2-44)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    self.passwordAgain.backgroundColor = [UIColor clearColor];
    _passwordAgain.delegate = self;
    _passwordAgain.secureTextEntry = YES;
    _passwordAgain.clearButtonMode = UITextFieldViewModeUnlessEditing;
    _passwordAgain.placeholder = @" 再次输入密码";
    [textback4View addSubview:_passwordAgain];
    
    self.creatAccount = [UIButton buttonWithType:UIButtonTypeSystem];
    self.creatAccount.frame = CGRectMake(40*Shitu_SCREENWIDTH/375, (257+212)*Shitu_SCREENHEIGHT/667, (590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667);
    [self.creatAccount setTitle:@"创建账户" forState:UIControlStateNormal];
    [self.creatAccount setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.creatAccount.backgroundColor = Shitu_ColorWithRgbValue(0x00baf0);
    self.creatAccount.titleLabel.font = [UIFont systemFontOfSize:17];
    
    [self.view addSubview:_welcome];
    [self.view addSubview:_creatAccount];
    
    [self.creatAccount addTarget:self action:@selector(getAccountAndPassword:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *loginLabel = [[UILabel alloc]initWithFrame:CGRectMake((258/2)*Shitu_SCREENWIDTH/375, 625*Shitu_SCREENHEIGHT/667, 90, 16)];
    loginLabel.text = @"已有账号？请";
    loginLabel.textColor = Shitu_ColorWithRgbValue(0x9a9a9a);
    loginLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:loginLabel];
    
    self.login = [UIButton buttonWithType:UIButtonTypeSystem];
    self.login.frame = CGRectMake((258/2)*Shitu_SCREENWIDTH/375+90, 625*Shitu_SCREENHEIGHT/667, 32, 16);
    self.login.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.login setTitleColor:Shitu_ColorWithRgbValue(0x00baf0) forState:UIControlStateNormal];
    [self.login setTitle:@"登录" forState:UIControlStateNormal];
    [self.login addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.login];
    
    //键盘观察者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillResign) name:UIApplicationWillEnterForegroundNotification object:nil];
    // Do any additional setup after loading the view.
}
-(void)keyboardWillResign
{
    [self.view endEditing:YES];
}
-(void)getAccountAndPassword:(id)sender
{
    
    if (![_account.text isEqualToString:@""]&&![_password.text isEqualToString:@""]&&![_email.text isEqualToString:@""]) {
        NSLog(@"已注册");
        if (![_password.text isEqualToString:_passwordAgain.text]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
                [self.view addSubview:hud];
                hud.labelText = @"两次密码不一致！";
                hud.mode = MBProgressHUDModeText;
                
                [hud showAnimated:YES whileExecutingBlock:^{
                    sleep(1);
                } completionBlock:^{
                    [hud removeFromSuperview];
                }];
            });
            return;
        }
        if (![self isValidateEmail:_email.text]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
                    [self.view addSubview:hud];
                    hud.labelText = @"邮箱不合法！";
                    hud.mode = MBProgressHUDModeText;
                    
                    [hud showAnimated:YES whileExecutingBlock:^{
                        sleep(1);
                    } completionBlock:^{
                        [hud removeFromSuperview];
                    }];
                });
                return;

        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        //申明返回的结果是json类型
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //申明请求的数据是json类型
        manager.requestSerializer=[AFJSONRequestSerializer serializer];
        //如果报接受类型不一致请替换一致text/html或别的
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        __block NSDictionary *tokenInfo = nil;
        //发送请求
        [manager POST:REGISTERURL parameters:@{@"name":_account.text,@"email":_email.text,@"password":_password.text} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            tokenInfo = [NSDictionary dictionaryWithDictionary:responseObject];
            NSLog(@"1---Success: %@,%@", tokenInfo,[NSThread currentThread]);
            
            if ([[tokenInfo objectForKey:@"register"]isEqualToString:@"success"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
                    
                    [self.view addSubview:hud];
                    hud.labelText = @"注册完成！请等待审核。";
                    hud.mode = MBProgressHUDModeText;
                    
                    [hud showAnimated:YES whileExecutingBlock:^{
                        sleep(1);
                    } completionBlock:^{
                        [hud removeFromSuperview];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:YES];
                        });
                    }];
                });
                
            }
            
            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"1---Error: %@", error);
            
            
        }];

       
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:hud];
            hud.labelText = @"请输入账户和密码！";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });

    }
    
}
-(BOOL)isValidateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)login:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 键盘处理
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGRect frame = self.view.frame;
    frame.origin.y +=100;
    frame.size.height -=100;
    self.view.frame = frame;
    _isTextOffset = NO;
    
    if ([_account isFirstResponder]) {
        [_account resignFirstResponder];
        [_email becomeFirstResponder];
    }
    else if ([_email isFirstResponder])
    {
        [_email resignFirstResponder];
        [_password becomeFirstResponder];
    }
    else if([_password isFirstResponder])
    {
        [_password resignFirstResponder];
        [_passwordAgain becomeFirstResponder];
        
    }
    else
    {
        [_passwordAgain resignFirstResponder];
        
    }
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_isTextOffset) {
        CGRect frame = self.view.frame;
        frame.origin.y +=100;
        frame.size.height -=100;
        self.view.frame = frame;
        _isTextOffset = NO;
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //开始编辑时触发，文本字段将成为first responder
    if (_isTextOffset == YES) {
        return;
    }
    //键盘遮住了文本字段，视图整体上移
    CGRect frame = self.view.frame;
    frame.origin.y -=100;
    frame.size.height +=100;
    self.view.frame = frame;
    _isTextOffset = YES;

}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if ((touch.tapCount == 1)&&([_account isFirstResponder]||[_password isFirstResponder]||[_email isFirstResponder]||[_passwordAgain isFirstResponder])) {
        
        
        CGRect frame = self.view.frame;
        frame.origin.y +=100;
        frame.size.height -=100;
        self.view.frame = frame;
        _isTextOffset = NO;
        
        [_account resignFirstResponder];
        [_password resignFirstResponder];
        [_email resignFirstResponder];
        [_passwordAgain resignFirstResponder];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
