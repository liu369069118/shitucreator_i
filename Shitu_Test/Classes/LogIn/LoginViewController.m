//
//  LoginViewController.m
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "LoginViewController.h"
#import "ContentViewController.h"
#import "RegistViewController.h"
#import "AccountTokenManager.h"
#import "MBProgressHUD.h"
//#import "JingRoundView.h"
@interface LoginViewController ()<UIActionSheetDelegate,loginStateDelegate>
@property(nonatomic,strong)UIButton *login;
@property(nonatomic,strong)UIButton *regist;
@property(nonatomic,strong)UIView *failView;
@property(nonatomic,assign)BOOL isTextOffset;
//@property(nonatomic,strong)JingRoundView *roundView;

@end

@implementation LoginViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.roundView.delegate = self;
//    self.roundView.roundImage = [UIImage imageNamed:@"login_circle"];
//    self.roundView.rotationDuration = 8.0;
//    self.roundView.isPlay = YES;
//    [self.view addSubview:self.roundView];
    
    UIImageView *background = [[UIImageView alloc]initWithFrame:self.view.bounds];
    background.image = [UIImage imageNamed:@"login_BG"];
    [self.view addSubview:background];
    
    UIImageView *title = [[UIImageView alloc]initWithFrame:CGRectMake(38*Shitu_SCREENWIDTH/375, (164/2)*Shitu_SCREENHEIGHT/667,300*Shitu_SCREENWIDTH/375, 50*Shitu_SCREENWIDTH/375)];
    title.image = [UIImage imageNamed:@"login_welcome"];
    [self.view addSubview:title];
    
    [AccountTokenManager shareAccountTokenManager].loginStateDelegate = self;
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(110*Shitu_SCREENWIDTH/375, (360/2)*Shitu_SCREENHEIGHT/667, (310/2)*Shitu_SCREENHEIGHT/667, (310/2)*Shitu_SCREENHEIGHT/667)];
    logo.image = [UIImage imageNamed:@"login_logo"];
    [self.view addSubview:logo];
    
    UIView *textback1View = [[UIView alloc]initWithFrame:CGRectMake(40*Shitu_SCREENWIDTH/375, (748/2)*Shitu_SCREENHEIGHT/667, (590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    textback1View.backgroundColor = Shitu_ColorWithRgbValue(0xf0f0f0);
    [self.view addSubview:textback1View];
    
    UIImageView *accountIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10*Shitu_SCREENHEIGHT/667, 29, 29)];
    accountIcon.image = [UIImage imageNamed:@"login_email_pic"];
    [textback1View addSubview:accountIcon];
    
    self.account = [[UITextField alloc]initWithFrame:CGRectMake(39,0, (590/2-44)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    _account.clearButtonMode = UITextFieldViewModeUnlessEditing;
    _account.backgroundColor = [UIColor clearColor];
    _account.delegate = self;
    _account.placeholder = @" 请输入邮箱";
    [textback1View addSubview:_account];
    
    UIView *textback2View = [[UIView alloc]initWithFrame:CGRectMake(40*Shitu_SCREENWIDTH/375, (866/2)*Shitu_SCREENHEIGHT/667, (590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    textback2View.backgroundColor = Shitu_ColorWithRgbValue(0xf0f0f0);
    [self.view addSubview:textback2View];
    
    UIImageView *pwdIcon = [[UIImageView alloc]initWithFrame:CGRectMake(10,10*Shitu_SCREENHEIGHT/667, 29, 29)];
    pwdIcon.image = [UIImage imageNamed:@"login_pwd_pic"];
    [textback2View addSubview:pwdIcon];
    
    self.password = [[UITextField alloc]initWithFrame:CGRectMake(39, 0, (590/2-44)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667)];
    self.password.backgroundColor = [UIColor clearColor];
    _password.delegate = self;
    _password.secureTextEntry = YES;
    _password.clearButtonMode = UITextFieldViewModeUnlessEditing;
    _password.placeholder = @" 请输入密码";
    [textback2View addSubview:_password];
    
    
    self.login = [UIButton buttonWithType:UIButtonTypeSystem];
    self.login.frame = CGRectMake(40*Shitu_SCREENWIDTH/375, 503*Shitu_SCREENHEIGHT/667,(590/2)*Shitu_SCREENWIDTH/375, 47*Shitu_SCREENHEIGHT/667);
    self.login.backgroundColor = Shitu_ColorWithRgbValue(0x00baf0);
    self.login.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.login setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.login setTitle:@"登 录" forState:UIControlStateNormal];
    
    self.regist = [UIButton buttonWithType:UIButtonTypeSystem];
    self.regist.frame = CGRectMake((254/2)*Shitu_SCREENWIDTH/375, 625*Shitu_SCREENHEIGHT/667, 32, 16);
    self.regist.titleLabel.font = [UIFont systemFontOfSize:15];
    self.regist.center = CGPointMake(Shitu_SCREENWIDTH/2, 625*Shitu_SCREENHEIGHT/667+8);
    [self.regist setTitleColor:Shitu_ColorWithRgbValue(0x00baf0) forState:UIControlStateNormal];
    [self.regist setTitle:@"注册" forState:UIControlStateNormal];
    
    UIButton *forget = [UIButton buttonWithType:UIButtonTypeSystem];
    forget.frame = CGRectMake((254/2+45)*Shitu_SCREENWIDTH/375, 625*Shitu_SCREENHEIGHT/667, 100, 16);
    [forget setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [forget setTitleColor:Shitu_ColorWithRgbValue(0x9a9a9a) forState:UIControlStateNormal];
    forget.titleLabel.font = [UIFont systemFontOfSize:15];
//    [self.view addSubview:forget];
    
    [self.view addSubview:_regist];
    [self.view addSubview:_login];
    
    [_login addTarget:self action:@selector(getAccountAndPassword:) forControlEvents:UIControlEventTouchUpInside];
    [_regist addTarget:self action:@selector(regist:) forControlEvents:UIControlEventTouchUpInside];
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardResign) name:UIApplicationWillEnterForegroundNotification object:nil];
    // Do any additional setup after loading the view.
}
-(void)keyboardResign
{
    [_account resignFirstResponder];
    [_password resignFirstResponder];
}
#pragma mark 注册
-(void)regist:(id)sender
{
    RegistViewController *registVc = [[RegistViewController alloc]init];
    [self.navigationController pushViewController:registVc animated:YES];
    
}
#pragma mark 键盘处理
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGRect frame = self.view.frame;
        frame.origin.y +=150;
        frame.size.height -=150;
        self.view.frame = frame;
    _isTextOffset = NO;
    if ([_account isFirstResponder]) {
        [_account resignFirstResponder];
        [_password becomeFirstResponder];
    }
    else
    {
        [_password resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //开始编辑时触发，文本字段将成为first responder
    if (_isTextOffset == YES) {
        return;
    }
    //键盘遮住了文本字段，视图整体上移
    CGRect frame = self.view.frame;
    frame.origin.y -=150;
    frame.size.height +=150;
    self.view.frame = frame;
    _isTextOffset = YES;
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_isTextOffset) {
        CGRect frame = self.view.frame;
        frame.origin.y +=150;
        frame.size.height -=150;
        self.view.frame = frame;
        _isTextOffset = NO;
    }
    
}

-(void)getAccountAndPassword:(id)sender
{
//    _account.text = @"shitunature@emebroad.com";
//    _password.text = @"Weibohao1409";
//    _account.text = @"test2@test.com";
    if ([_account.text isEqualToString:@""]||[_password.text isEqualToString:@""]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:hud];
            hud.labelText = @"请输入账户和密码！";
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });

    }
    else
    {
        NSLog(@"已输入账户名和密码");
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"token"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"authState"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"authFailed"];
        
       
        [[AccountTokenManager shareAccountTokenManager]TokenWithPostURL:POSTTOKEN AccountData:[NSMutableDictionary dictionaryWithDictionary:@{@"email":_account.text,@"password":_password.text}]];
    }
    
}
-(void)auth{
    
    if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"auth"]isEqualToString:@"ok"]) {
      
        NSString *token = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"token"];
        
        [[AccountTokenManager shareAccountTokenManager]authWithLoginURL:VERIFYTOKEN authData:[NSMutableDictionary dictionaryWithDictionary:@{@"t":token}]];
    }
    else if ([[[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"auth"]isEqualToString:@"failed"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:hud];
            hud.labelText = [[[NSUserDefaults standardUserDefaults]objectForKey:@"token"]objectForKey:@"msg"];
            hud.mode = MBProgressHUDModeText;
            
            [hud showAnimated:YES whileExecutingBlock:^{
                sleep(2);
            } completionBlock:^{
                [hud removeFromSuperview];
            }];
        });
        
    }
    
}

-(void)authSuccessAndlogin
{
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"authState"]isEqualToString:@"ok"]) {

        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
}
-(void)authFailed
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:hud];
        hud.labelText = @"验证失败";
        hud.mode = MBProgressHUDModeText;
        
        [hud showAnimated:YES whileExecutingBlock:^{
            sleep(2);
        } completionBlock:^{
            [hud removeFromSuperview];
        }];
    });

    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if ((touch.tapCount == 1)&&([_account isFirstResponder]||[_password isFirstResponder])) {
        
            
            CGRect frame = self.view.frame;
            frame.origin.y +=150;
            frame.size.height -=150;
            self.view.frame = frame;
            _isTextOffset = NO;
        
        [_account resignFirstResponder];
        [_password resignFirstResponder];
        
    }
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
