//
//  AccountTokenManager.m
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "AccountTokenManager.h"
#import "AFHTTPRequestOperationManager.h"


@implementation AccountTokenManager
+(AccountTokenManager *)shareAccountTokenManager
{
    
    static AccountTokenManager *accountTokenManager = nil;

    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        accountTokenManager = [[self alloc] init];
    });
    return accountTokenManager;
}
-(void )TokenWithPostURL:(NSString *)postURL AccountData:(NSMutableDictionary *)accountData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //申明请求的数据是json类型
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    //如果报接受类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    __block NSObject *tokenInfo = nil;
    //发送请求
    [manager POST:postURL parameters:accountData success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        NSLog(@"1---Success: %@,%@", responseObject,[NSThread currentThread]);
        tokenInfo = responseObject;
        [self passTokenWithToken:tokenInfo];
        [self postSuccess];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"1---Error: %@", error);
        [self.loginStateDelegate authFailed];

    }];
}

-(void )passTokenWithToken:(NSObject *)tokenInfo
{
   
    [[NSUserDefaults standardUserDefaults]setObject:tokenInfo forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)postSuccess
{
    [self.loginStateDelegate auth];
}

-(void )authWithLoginURL:(NSString *)loginURL authData:(NSMutableDictionary *)authData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //申明请求的数据是json类型
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    //如果报接受类型不一致请替换一致text/html或别的
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    __block NSObject *tokenInfo = nil;
    //发送请求
    [manager POST:loginURL parameters:authData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"2---Success: %@,%@", responseObject,[NSThread currentThread]);
        tokenInfo = [responseObject objectForKey:@"auth"];
        [self passAuthWithToken:tokenInfo];
        [self authSuccess];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"2---Error: %@", error);
        [self.loginStateDelegate authFailed];
    }];
}
-(void )passAuthWithToken:(NSObject *)tokenInfo
{
        [[NSUserDefaults standardUserDefaults]setObject:tokenInfo forKey:@"authState"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    
}

-(void)authSuccess
{
    [self.loginStateDelegate authSuccessAndlogin];
}
@end
