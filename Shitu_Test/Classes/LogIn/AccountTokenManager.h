//
//  AccountTokenManager.h
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol loginStateDelegate <NSObject>

-(void)auth;
-(void)authSuccessAndlogin;
-(void)authFailed;
@end
@interface AccountTokenManager : NSObject
+(AccountTokenManager *)shareAccountTokenManager;
-(void )TokenWithPostURL:(NSString *)postURL AccountData:(NSMutableDictionary *)accountData;
-(void )authWithLoginURL:(NSString *)loginURL authData:(NSMutableDictionary *)authData;
@property(assign,nonatomic)id<loginStateDelegate> loginStateDelegate;
@end
