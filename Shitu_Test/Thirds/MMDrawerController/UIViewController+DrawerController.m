// Copyright (c) 2013 Mutual Mobile (http://mutualmobile.com/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#import "UIViewController+DrawerController.h"

@implementation UIViewController (DrawerController)


-(DrawerController*)_drawerController{
    if([self.parentViewController isKindOfClass:[DrawerController class]]){
        return (DrawerController*)self.parentViewController;
    }
    else if([self.parentViewController isKindOfClass:[UINavigationController class]] &&
            [self.parentViewController.parentViewController isKindOfClass:[DrawerController class]]){
        return (DrawerController*)[self.parentViewController parentViewController];
    }
    else{
        return nil;
    }
}

-(CGRect)_visibleDrawerFrame{
    if([self isEqual:self._drawerController.leftDrawerViewController] ||
       [self.navigationController isEqual:self._drawerController.leftDrawerViewController]){
        CGRect rect = self._drawerController.view.bounds;
        rect.size.width = self._drawerController.maximumLeftDrawerWidth;
        return rect;
        
    }
    else if([self isEqual:self._drawerController.rightDrawerViewController] ||
             [self.navigationController isEqual:self._drawerController.rightDrawerViewController]){
        CGRect rect = self._drawerController.view.bounds;
        rect.size.width = self._drawerController.maximumRightDrawerWidth;
        rect.origin.x = CGRectGetWidth(self._drawerController.view.bounds)-rect.size.width;
        return rect;
    }
    else {
        return CGRectNull;
    }
}

@end
