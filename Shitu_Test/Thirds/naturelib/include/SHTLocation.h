//
//  SHTLocation.h
//  naturelib
//
//  Created by shitupublic on 15/5/13.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//
#import "SHTSensors.h"
#import "SHTStepDelegate.h"

@class SHTNatureParameters, SHTPosition;

@protocol SHTLocationRespondeDelegate <NSObject>

@required
- (void) getLocation: (SHTPosition *) position;
- (void) getStatus: (int)status Message: (NSString *)message;
- (void) getDebugInfo: (NSString *) debug;
- (void) getAngle: (int) angle;
- (void) getRadius: (int) radius;
- (void) getFloor: (int) floor;
- (void) onWalk;
- (void) onStopWalk;
- (void) onError: (NSError *) error;
@end

@interface SHTLocation : NSObject<SHTSensorDelegate, SHTStepDelegate>

@property (nonatomic, weak) id<SHTLocationRespondeDelegate> delegate;

- (instancetype) initWithNatureParameters: (SHTNatureParameters *) parameters;

- (void)start;

- (void)stop;

@end
