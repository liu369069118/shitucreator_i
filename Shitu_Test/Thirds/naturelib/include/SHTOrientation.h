//
//  SHTOrientation.h
//  naturelib
//
//  Created by shitupublic on 15/5/7.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//


@interface SHTOrientation : NSObject

@property (nonatomic) float azimuth;
@property (nonatomic) float pitch;
@property (nonatomic) float roll;

- (instancetype) initWithAzimuth: (float)azimuth Pitch: (float)pitch Roll:(float)roll;

@end

