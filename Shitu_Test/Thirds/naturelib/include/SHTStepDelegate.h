//
//  SHTStepDelegate.h
//  naturelib
//
//  Created by shitupublic on 15/6/18.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//

@protocol SHTStepDelegate <NSObject>

- (void)onStep: (int)step;

@end
