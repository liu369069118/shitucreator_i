//
//  SHTSocketData.h
//  naturelib
//
//  Created by shitupublic on 15/6/8.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//

#ifndef naturelib_SHTSocketDataType_h
#define naturelib_SHTSocketDataType_h

// data
typedef unsigned char byte;

// data with out any extra info such as length.
typedef byte * dataonly;

typedef byte * datapacked;

// raw data in bytes, may or may not packed;
typedef byte * rawdata;



#endif
