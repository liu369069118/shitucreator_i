//
//  SHTByteable.h
//  naturelib
//
//  Created by shitupublic on 15/5/7.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//

#import "SHTSocketDataType.h"

/**
 *  bundle data protocol which is used in socket transmission.
 */
@protocol SHTByteable <NSObject>

@required

/**
 * @brief bundle the data in the format: [dataByteSize(int), dataThemselves(byte array)];
 */
- (datapacked) mallocBundle;

- (long) bundleSize;

@end

