//
//  FloatEncode.h
//  naturelib
//
//  Created by shitupublic on 15/4/29.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//

@interface SHTSmallFloat : NSObject

@property (nonatomic) float floatValue;
@property (nonatomic) short shortValue;

- (instancetype)initWithFloat:(float)floatValue;
- (instancetype)initWithShort:(short)shortValue;

+ (short) toShortWithSmallFloat: (float) floatValue;

@end

