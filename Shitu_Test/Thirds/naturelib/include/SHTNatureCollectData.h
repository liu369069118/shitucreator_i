//
//  SHTNatureCollectData.h
//  naturelib
//
//  Created by shitupublic on 15/5/21.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//
#import "SHTNatureData.h"

@interface SHTNatureCollectData : SHTNatureData

@property (nonatomic) unsigned char isend;
@property (nonatomic) int rule;
@property (nonatomic) int area;
@property (nonatomic) float px;
@property (nonatomic) float py;
@property (nonatomic) int line;
@property (nonatomic) int node;

@end
