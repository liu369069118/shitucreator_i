//
//  SHTLocationCloud.h
//  naturelib
//
//  Created by shitupublic on 15/6/15.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//
#import "SHTLocation.h"
#import "SHTLocationEx.h"
#import "SHTPositionSocketDelegate.h"

@protocol SHTLocationCloudDelegate <NSObject>

- (void)onNetProblem:(NSException *) exception;

@end


@interface SHTLocationCloud : SHTLocation<SHTPositionSocketDelegate>
    
@property (nonatomic, weak) id<SHTLocationCloudDelegate> netDelegate;

@end
