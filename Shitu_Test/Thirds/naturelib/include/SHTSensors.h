//
//  SHTSensors.h
//  naturelib
//
//  Created by shitupublic on 15/5/12.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//


#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>


@class CMDeviceMotion;

@protocol SHTSensorDelegate <NSObject>

@optional
- (void)didGotDeviceMotionData: (CMDeviceMotion *)motion OrError: (NSError *)error;
- (void)didGotHeadingData: (CLHeading *)heading;
- (void)didGotMagnetometerData: (CMMagnetometerData *)magneticData OrError: (NSError *)error;
- (void)didGotPedometerData:(CMPedometerData *)pedometerData OrError: (NSError *)error;
- (void)magnetometerFromDeviceMotionUnsupported;
- (void)magnetometerFromDeviceMotionUnreliable;
- (void)headingInfoUnreliable;
- (void)didGotAccelerometer:(CMAccelerometerData *)accData OrError: (NSError *)error;
- (void)didGotGyro: (CMGyroData *)gyroData OrError: (NSError *)error;

@end

@interface SHTSensors : NSObject<CLLocationManagerDelegate>

@property (nonatomic) NSTimeInterval interval;
@property (nonatomic, weak) id<SHTSensorDelegate> delegate;

- (instancetype) initWithInterval: (NSTimeInterval) interval;

- (BOOL)registerMagnetometersSensor;

- (BOOL)registerCompass;

- (BOOL)registerDeviceMotionSensor;

- (BOOL)registerGyro;

- (BOOL)registerAccelerator;

- (BOOL)registerPedometer;

- (BOOL)supportFloorCounting;

- (BOOL)supportDistanceCounting;

- (void)start;

- (void)unregisterSensors;

- (void)stop;

@end