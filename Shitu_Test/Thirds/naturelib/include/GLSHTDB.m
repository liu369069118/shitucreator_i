//
//  GLSHTDB.m
//  Shitu_Test
//
//  Created by shitupublic on 15/6/29.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "GLSHTDB.h"

@implementation GLSHTDB
-(DrawPath *)selectNodeFromDBWithArea:(int)area line:(int)line
{
    
    
    NSString * lineSQL = [NSString stringWithFormat:@"SELECT * FROM mag WHERE area = %d AND node = 1 AND line = %d", area,line];
    
    [self prepareStatement:lineSQL];
    
    DrawPath *drawPath = [[DrawPath alloc]init];
    
    drawPath.nodes = [NSMutableArray array];
    while ([self execPrepareStatment]==SQLITE_ROW)
    {
        float nodeX=[self getDoubleAtColumn:3];
        float nodeY=[self getDoubleAtColumn:4];
        int whichLine = [self getIntAtColumn:10];
        
        CGPoint point = CGPointMake(nodeX, nodeY);
        
        
        drawPath.whichLine = whichLine;
        [drawPath.nodes addObject:[NSValue valueWithCGPoint:point]];
    }
    
    NSLog(@"lineArray = %@",drawPath.nodes);
    
    [self deletePrepareStmt];
    return drawPath;
}

-(NSMutableArray *)selectLinesWithArea:(int )area
{
    NSString * lineSQL = [NSString stringWithFormat:@"SELECT distinct line FROM mag WHERE area = %d AND node = 1", area];
    
    [self prepareStatement:lineSQL];
    
    NSMutableArray *array = [NSMutableArray array];
    
    while ([self execPrepareStatment]==SQLITE_ROW)
    {
        
        int whichLine = [self getIntAtColumn:0];
        [array addObject:[NSNumber numberWithInt:whichLine]];
    }
    return array;
}
-(int)maxLine:(int )area
{
    NSString * countSQL = [NSString stringWithFormat:@"SELECT MAX(line) FROM mag WHERE area = %d",area];
    [self prepareStatement:countSQL];
    int maxLine = 0;
    
    int status = [self execPrepareStatment];
    if(status == SQLITE_OK || status == SQLITE_DONE || status == SQLITE_ROW){
        maxLine = [self getIntAtColumn:0];
    }else{
        NSLog(@"max line error Table error for");
    }
    
    [self deletePrepareStmt];
    
    return maxLine;
}
- (int)countOfFloor: (int)area
{
    
    NSString * countSQL = [NSString stringWithFormat:@"SELECT COUNT(*) FROM mag WHERE area = %d LIMIT 1", area];
    [self prepareStatement:countSQL];
    int count = 0;
    
    int status = [self execPrepareStatment];
    if(status == SQLITE_OK || status == SQLITE_DONE || status == SQLITE_ROW){
        count = [self getIntAtColumn:0];
    }else{
        NSLog(@"countOfFloor error for");
    }
    
    [self deletePrepareStmt];
    
    return count;
}
-(BOOL)deleteLine:(int)line fromArea:(int)area
{
    NSString *deleteMagSQL = [NSString stringWithFormat:@"DELETE FROM mag WHERE area = %d AND line = %d",area,line];
    
    if ([self beginTransaction]) {
        BOOL test1 = [self execSQL:deleteMagSQL];
        if (test1) {
            return [self commit];
        }
        else{
            [self rollback];
            return NO;
        }
    }
    return NO;
}
-(int)selectMaxRuleWithDeleteLine:(int)line area:(int)area
{
    NSString * countSQL = [NSString stringWithFormat:@"SELECT MAX(rule) FROM mag WHERE line = %d and area = %d",line,area];
    [self prepareStatement:countSQL];
    int maxRule = 0;
    
    int status = [self execPrepareStatment];
    if(status == SQLITE_OK || status == SQLITE_DONE || status == SQLITE_ROW){
        maxRule = [self getIntAtColumn:0];
    }else{
        NSLog(@"max rule error Table error for");
    }
    
    [self deletePrepareStmt];
    
    return maxRule;
}
-(int)countOfDeleteRule:(int)line area:(int)area
{
    NSString * countSQL = [NSString stringWithFormat:@"SELECT COUNT(*) FROM mag WHERE area = %d AND line = %d", area,line];
    [self prepareStatement:countSQL];
    int count = 0;
    
    int status = [self execPrepareStatment];
    if(status == SQLITE_OK || status == SQLITE_DONE || status == SQLITE_ROW){
        count = [self getIntAtColumn:0];
    }else{
        NSLog(@"countOfFloor error for");
    }
    
    [self deletePrepareStmt];
    
    return count;
}
-(BOOL )updateDataBaseInOrderMaxRuleWithDeleteLine:(int)line count:(int)count
{
    
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE mag SET rule = rule - %d WHERE rule > %d",count,line];
    NSLog(@"delete----UpdateSQL = %@",updateSQL);
    if ([self beginTransaction]) {
        NSLog(@"执行前");
        BOOL test = [self execSQL:updateSQL];
        if (test) {
            NSLog(@"执行成功");
            BOOL state = [self commit];
            return state;
        }
        else{
            NSLog(@"执行失败");
            [self rollback];
            return NO;
        }
    }
    return NO;

    
}
-(BOOL)isRuleNotInOrder:(int)area
{
    if ([self maxRuleWithArea:area]>0&&[self maxRuleWithArea:area]<[self maxRule]) {
        return YES;
    }
    return NO;
}
-(int )maxRuleWithArea:(int )area
{
    NSString * countSQL = [NSString stringWithFormat:@"SELECT MAX(rule) FROM mag WHERE area = %d",area];
    [self prepareStatement:countSQL];
    int maxRule = 0;
    
    int status = [self execPrepareStatment];
    if(status == SQLITE_OK || status == SQLITE_DONE || status == SQLITE_ROW){
        maxRule = [self getIntAtColumn:0];
    }else{
        NSLog(@"max rule error for area");
    }
    
    [self deletePrepareStmt];
    
    return maxRule;

}
-(int )countOfOtherRule:(int )area
{
    NSString * countSQL = [NSString stringWithFormat:@"SELECT COUNT(*) FROM mag WHERE rule > %d LIMIT 1", [self maxRuleWithArea:area]];
    [self prepareStatement:countSQL];
    int count = 0;
    
    int status = [self execPrepareStatment];
    if(status == SQLITE_OK || status == SQLITE_DONE || status == SQLITE_ROW){
        count = [self getIntAtColumn:0];
    }else{
        NSLog(@"countOfOtherRule error for");
    }
    
    [self deletePrepareStmt];
    
    return count;

}
-(BOOL)updateDataBaseInOrderWithMaxRule:(int )maxRule recordsCount:(int )recordsCount
{
    
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE mag SET rule = rule + %d WHERE rule > %d",recordsCount,maxRule];
        
        if ([self beginTransaction]) {
            BOOL test = [self execSQL:updateSQL];
            if (test) {
                return [self commit];
            }
            else{
                [self rollback];
                return NO;
            }
        }
        return NO;
    
}
-(BOOL)updateDataBaseInOrderWithOtherRule:(int )area count:(int )count otherCount:(int )otherCount
{
    
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE mag SET rule = rule - %d WHERE rule > %d and area = %d",otherCount,count,area];
    
    if ([self beginTransaction]) {
        BOOL test = [self execSQL:updateSQL];
        if (test) {
            return [self commit];
        }
        else{
            [self rollback];
            return NO;
        }
    }
    return NO;
    

}
#pragma mark 反向功能
//-(DrawPath *)reverseDirectionWithArea:(int )area Line:(int )line
//{
//    NSString * reverseSQL = [NSString stringWithFormat:@"SELECT * FROM mag WHERE area = %d AND line = %d order by desc", area,line];
//    
//    [self prepareStatement:reverseSQL];
//    
//    DrawPath *drawPath = [[DrawPath alloc]init];
//    
//    drawPath.nodes = [NSMutableArray array];
//    while ([self execPrepareStatment] == SQLITE_ROW)
//    {
//        float nodeX=[self getDoubleAtColumn:3];
//        float nodeY=[self getDoubleAtColumn:4];
//        int whichLine = [self getIntAtColumn:10];
//        
//        CGPoint point = CGPointMake(nodeX, nodeY);
//        
//        
//        drawPath.whichLine = whichLine;
//        [drawPath.nodes addObject:[NSValue valueWithCGPoint:point]];
//    }
//    
//    NSLog(@"lineArray = %@",drawPath.nodes);
//    
//    [self deletePrepareStmt];
//    return drawPath;
//
//    
//}

@end
