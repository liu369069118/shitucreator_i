//
//  Position.h
//  naturelib
//  Model which represent the position of Nature Location.
//  Author: Yang Tao
//  Created by shitupublic on 15/5/4.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//

#import "SHTByteable.h"

@interface SHTPosition : NSObject<SHTByteable>


@property (nonatomic) int rule;

@property (nonatomic) short area;

@property (nonatomic) float x;

@property (nonatomic) float y;

@property (nonatomic) short source;

@property (nonatomic) float distance;

@property (nonatomic) int angle;

@property (nonatomic) int radius;

@property (nonatomic) int status;

/**
 *
 */
- (instancetype) initWithPositionBytes:(unsigned char[]) byteArray Size:(int)arraySize;

/*
 *
 */
- (instancetype) initWithX:(float)x Y:(float)y Rule:(int)rule Area:(short)area Distance:(float)distance Status:(int)status;

/*
 *
 */
- (BOOL)isOK;

- (BOOL)isDiscard;

- (BOOL)isStep;

- (BOOL)isNoMatch;


@end

