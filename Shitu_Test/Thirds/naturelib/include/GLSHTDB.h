//
//  GLSHTDB.h
//  Shitu_Test
//
//  Created by shitupublic on 15/6/29.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import "SHTNatureDB.h"
#import "DrawPath.h"
#import <sqlite3.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
@interface GLSHTDB : SHTNatureDB

-(int)maxLine:(int )area;

- (int)countOfFloor: (int)area;

-(BOOL)deleteLine:(int)line fromArea:(int)area;

-(DrawPath *)selectNodeFromDBWithArea:(int)area line:(int)line;

-(NSMutableArray *)selectLinesWithArea:(int )area;

-(BOOL)isRuleNotInOrder:(int)area;

-(int )maxRuleWithArea:(int )area;

-(int )countOfOtherRule:(int )area;

-(BOOL)updateDataBaseInOrderWithMaxRule:(int )maxRule recordsCount:(int )recordsCount;

-(BOOL)updateDataBaseInOrderWithOtherRule:(int )area count:(int )count otherCount:(int )otherCount;

-(int)countOfDeleteRule:(int)line area:(int)area;

-(BOOL )updateDataBaseInOrderMaxRuleWithDeleteLine:(int)line count:(int)count;

-(int)selectMaxRuleWithDeleteLine:(int)line area:(int)area;

@end
