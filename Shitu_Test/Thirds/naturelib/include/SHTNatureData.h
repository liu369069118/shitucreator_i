//
//  SHTNatureData.h
//  naturelib
//
//  Created by shitupublic on 15/5/7.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//


@class SHTOrientation;

@interface SHTNatureData : NSObject

@property (nonatomic) short x;
@property (nonatomic) short y;
@property (nonatomic) short z;
@property (nonatomic) float azimuth;
@property (nonatomic) float pitch;
@property (nonatomic) float roll;
@property (nonatomic) float pressure;

- (instancetype) initWithMagX:(float) magX AndMagY: (float)magY AndMagZ: (float)magZ AndOrientation:(SHTOrientation *)orientation Pressure:(float)pressure;

@end

