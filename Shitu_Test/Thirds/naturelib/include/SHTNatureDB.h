//
//  SHTNatureDB.h
//  naturelib
//
//  Created by shitupublic on 15/5/21.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//

#import "SHTDB.h"

@class SHTNatureCollectData, SHTUpdateDataSocket;

@interface SHTNatureDB : SHTDB

- (BOOL) createNesscery;

- (BOOL) insertMany: (NSArray *) natureCollectSet;

- (BOOL) insertToCollectWithArea: (int)area Line: (int) line Reverse: (int)reverse Related: (int)related;

- (BOOL) insertToPresWithPres: (float)pres Area: (int)area;

- (BOOL) openWithStoreId: (int)storeId;

- (BOOL) overrideCloud;

- (BOOL) appendToCloud;

- (BOOL) deleteCloud;

- (BOOL) deleteAllLocal;

- (BOOL) deleteArea: (int)area;

- (int)maxRule;

@end