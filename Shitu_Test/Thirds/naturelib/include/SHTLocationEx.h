//
//  SHTLocationEx.h
//  naturelib
//
//  Created by shitupublic on 15/5/18.
//  Copyright (c) 2015年 shitupublic. All rights reserved.
//
@class SHTPosition;

@interface SHTLocation ()

-(SHTPosition *)caculateWithX: (short *)xPt Y: (short *)yPt Z: (short *)zPt Azimuth: (float *)azimuthPt Pressure: (float)avgPressure Pitch: (float)avgPitch Useonewaymatch: (BOOL) useOnewayMatch WithSize: (int) sz AndStepX: (float)stepX StepY: (float)stepY;

- (void) heartbeat;

@end
