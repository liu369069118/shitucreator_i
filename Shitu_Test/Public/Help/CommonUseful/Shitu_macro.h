//
//  Shitu_macro.h
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

//颜色设置
#define Shitu_ColorRGB(r,g,b,a) [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:(a)]

#define Shitu_ColorWithRgbValue(rgbValue) [UIColor colorWithRed:((float)((rgbValue &0xFF0000) >>16))/255.0 green:((float)((rgbValue &0xFF00) >> 8))/255.0 blue:((float)((rgbValue &0xFF)))/255.0 alpha:1.0]


//各类常用尺寸
#define Shitu_SCREENWIDTH [[UIScreen mainScreen] bounds].size.width    //屏幕宽度

#define Shitu_SCREENHEIGHT [[UIScreen mainScreen] bounds].size.height  //屏幕长度

#define Shitu_FRAMEWIDTH self.frame.size.width    //子视图中的frame宽度

#define Shitu_FRAMEHEIGHT self.frame.size.height  //子视图中得frame高度
//旋转角度转化

#define degreesToRadians(x) (M_PI*(x)/180.0)

//常用
//#define Token @"token"
//#define t @"t"
//#define NSUserDefault(Key) [[NSUserDefaults standardUserDefaults]objectForKey:Key]