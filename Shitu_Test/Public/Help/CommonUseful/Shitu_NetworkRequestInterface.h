//
//  Shitu_NetworkRequestInterface.h
//  Shitu_Test
//
//  Created by publicshitu on 15/4/28.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#define POSTTOKEN @"http://open.ubirouting.com/auth/token"

#define VERIFYTOKEN @"http://open.ubirouting.com/napi/login"

#define REGISTERURL @"http://open.ubirouting.com/auth/register"

#define STOREINFO @"http://open.ubirouting.com/napi/allstores"

#define NewStore @"http://open.ubirouting.com/napi/newstore"

#define NewFloor @"http://open.ubirouting.com/napi/newfloor"

#define Disclaimer @"http://open.ubirouting.com/ab/exception"

