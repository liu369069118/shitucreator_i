//
//  BaseMapViewController.h
//  Shitu_Test
//
//  Created by publicshitu on 15/5/16.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI/BMapKit.h>
@interface BaseMapViewController : UIViewController<UIGestureRecognizerDelegate,BMKMapViewDelegate,BMKLocationServiceDelegate>
@property (nonatomic,strong )BMKMapView *mapView;
@property(nonatomic,strong)UIButton *startBtn;
@property(nonatomic,strong)UIButton *stopBtn;
@property(nonatomic,strong)UIButton *followingBtn;
@property(nonatomic,strong)UIButton *followHeadBtn;
@property(nonatomic,strong)BMKLocationService *locService;
@property(nonatomic,strong)BMKGroundOverlay *overlay;
@property(nonatomic,strong)BMKPointAnnotation *pointAnnotation;
-(void)startLocation:(id)sender;
-(void)stopLocation:(id)sender;
-(void)startFollowing:(id)sender;
-(void)startFollowHeading:(id)sender;
@end
