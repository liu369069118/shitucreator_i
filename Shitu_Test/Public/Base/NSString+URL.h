//
//  NSString+URL.h
//  Shitu_Test
//
//  Created by publicshitu on 15/6/12.
//  Copyright (c) 2015年 publicshitu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URL)
- (NSString *)URLEncodedString;
@end
